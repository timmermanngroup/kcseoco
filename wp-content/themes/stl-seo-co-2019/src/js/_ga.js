
//@prepros-prepend 	_jquery.ga-lib.js
//Add your Google Analytics Events below
//ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);

//CATEGORY: Contact

jQuery(document).ready( function(){

    const debug = false;
	
    //CONTACT
    $('a[href^="tel:"], a[href^="mailto:"]').each( function(){
        $(this).gaSendEvent( 'click', {}, debug );
    } );

    $('form').each( function(){
        
        var label = $(this).find('.ga-form input[type="hidden"]').val();
        
        if (label == 'CTA Audit Request'){
            label = 'Request an Audit';
        }else if(label == 'Contact Us'){
            label = 'Contact Form';
        }
        $(this).gaSendEvent( 'submit', {
            beforeLabel: 'Form Completion | ',
            label: label,
            }, 
            debug );
    } );


    //CATEGORY: Downloads
    $('.ga-download').each( function(){
        $(this).gaSendEvent( 'click', {}, debug );
    } );

    //Ranking Check Clicks
    $('.component__content.component--card-ranking .btn').each(function (e) {
        
        var button  = $(this);
        var clientName = $(this).parent().siblings('.card-header').find('.card--client').html();
        var keywords  = $(this).parent().siblings('.card-content').find('.card--keywords').html();
        var gLabel      = clientName + ' | ' + keywords;
        
        $(this).gaSendEvent('click', {
            category: 'Verify',
            beforeLabel: 'Verify the Ranking Click | ',
            label: gLabel,
        }, debug);

    });
    
    //Testimonial Module Interaction Clicks
    $('.module--carousel.carousel_testimonial').each(function () {
        $(this).gaSendEvent('translate.owl.carousel', {
            category: 'Testimonial',
            label: 'Testimonial Click |',
            action: 'Click',
        }, debug);
    });

    // FORM ERRORSs
    if( $('.gform_validation_error').length > 0 ) {
        let formName = $('.gform_validation_error').find( '.ga-form input[type="hidden"]' ).val();

        $( '.gfield_error' ).each( function(){
            let $this = $(this);
            let label = $this.find( '.gfield_label' ).text();
            let error = $this.find( '.validation_message' ).text();
            let eventLabel = 'Form Error | ' + formName + ' | ' + label + ' | ' + error;
            
            ga( 'send', 'event', 'Contact', 'Submit', eventLabel );

        } );
    }

} );