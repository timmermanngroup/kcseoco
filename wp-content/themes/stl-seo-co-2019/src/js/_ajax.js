jQuery(document).ready( function($) {

var $ajaxButton = $('.js-button');
var $container = $('#js-output');

$ajaxButton.on( 'click', function() {

	var interval = $ajaxButton.data( 'interval' );
	var count = $container.data( 'count' );
	var offset = parseInt( interval + count );

	$.ajax( {
		url: foobarAjax.ajaxurl,
		type: 'POST',
		data: {
			action: 'foobar_ajax_request',
			interval: interval,
			offset: count,
		},		
		success: function( response ) {

			if( parseInt( response ) === 0 ) {
				console.log( 'No more posts to display.' );

			} else {
				$container.data( 'count', offset );
				$container.append( response );
			}
				
		},
		error: function( response ) {
			console.log( response );
		}
	} ); // END AJAX

} );



} ); //END JQUERY SAFE-MODE