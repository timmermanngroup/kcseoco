jQuery(document).ready(function ($) {

    // When the user scrolls the page, execute myFunction 
    window.onscroll = function () { myFunction() };

    // Get the navbar
    var navbar = document.getElementById("drawer-nav");

    // Get the offset position of the navbar then add distance of scroll in pixels
    var sticky = parseInt(navbar.offsetTop, 10) + 100;

    // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {

        if (window.pageYOffset > sticky) {
            navbar.classList.add("pos-fixed")
        } else {
            navbar.classList.remove("pos-fixed");
        }
    }

    myFunction();
});