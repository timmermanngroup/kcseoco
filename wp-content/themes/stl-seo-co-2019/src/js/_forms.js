jQuery(document).ready(function ($) {
    var $form_inputs                = $('.gform_wrapper input:not([type=radio]):not([type=checkbox])');
    var $form_textareas             = $('.gform_wrapper textarea');
    var $complex_form_inputs        = $('.ginput_complex input:not([type=radio]):not([type=checkbox])');
    
    $form_inputs                    = $( $form_inputs.not( $complex_form_inputs ) );
    
    /*
    * Toggles Active class on the form label
    */
    function toggleActiveClass( $form_element, $form_element_label) {
        $form_element.focus( function (){
            $form_element_label.addClass("active");
        })
        $form_element.blur(function () {
            if (  $form_element.is('TEXTAREA')  ){
                if ( $form_element.val() == "") {
                    $form_element_label.removeClass("active");
                }
                if ($form_element.val() != "") {
                    $form_element.addClass("filled");
                }
                
            } else {
                if ($form_element.val() == "" || $form_element.val() == "(___) ___-____") {
                    $form_element_label.removeClass("active");
                }
                if ($form_element.val() != "") {
                    $form_element.addClass("filled");
                }
            }
        })
    }
    
    $complex_form_inputs.each( function(){
        var $form_element           = $(this);
        var $form_element_label     = $(this).parent().children('label');
        
        toggleActiveClass( $form_element, $form_element_label );
    })

    $form_inputs.each(function () {
        var $form_element       = $(this);
        var $form_element_label = $(this).parent().prev();
        
        toggleActiveClass($form_element, $form_element_label);
    })

    $form_textareas.each(function () {
        var $form_element = $(this);
        var $form_element_label = $(this).parent().prev();

        toggleActiveClass($form_element, $form_element_label);
    })
});


