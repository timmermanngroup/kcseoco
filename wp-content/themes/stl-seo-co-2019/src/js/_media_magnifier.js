jQuery(document).ready(function ($) {

    var bannerModules = $('.module__content.module--image-banner');
    
    bannerModules.each( function () {

        var bannerImg           = $(this).find('.banner--image img');
        var bannerMagnifiers    = $(this).find('.banner--magnifier');


        bannerMagnifiers.each( function() {

            var bannerMagnifier     = $(this);
            var imageSrc            = bannerImg.attr('src');
            var imageWidth          = bannerImg.attr('width');
            var imageHeight         = bannerImg.attr('height');
            var backgroundWidth     = imageWidth * 1.5;
            var backgroundHeight    = imageHeight * 1.5; 
            var leftOffset          = bannerMagnifier.offset().left;
            var topOffset           = bannerMagnifier.offset().top;

            var distanceFromLeft    = -Math.abs(leftOffset * .625);
            var distanceFromTop     = -Math.abs(topOffset * .169 );
            
            // console.log(bannerImg);
            // console.log(bannerImg.attr('src'));
            // console.log(bannerMagnifier);
            // console.log('banner left offset is:' + leftOffset);
            // console.log('banner top offset is:' + topOffset);
            
            bannerMagnifier.css('background-image', 'url(' + imageSrc  + ')' );
            bannerMagnifier.css('background-repeat', 'no-repeat' );
            bannerMagnifier.css('background-size', backgroundWidth + "px " + backgroundHeight + "px");
            bannerMagnifier.css('background-position', distanceFromLeft + "px " + distanceFromTop + "px");
        })
        
    });

});