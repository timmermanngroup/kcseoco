/*
 * This is a Google Analytics Plugin developed
 * by Timmermann Group to help with sending
 * Google Events.
*/

function gaGetBeforeLabel( element, args ) {
	var beforeLabel;
	// Check and set the beforeLabel Property
	// If the beforeLabel parameter is passed in args..
	if( args && args.beforeLabel ) {
		beforeLabel = args.beforeLabel;

	} else if( element.data( 'before-label' ) ) {
		//.. else if the element has the data-before-label proprty set..
		beforeLabel = element.data( 'before-label' );

	} else if( element.is( 'a[href^="tel:"]' ) ) {
		//.. else if the element is a phone link..
		beforeLabel = 'Phone Number Clicked | ';

	} else if( element.is( 'a[href^="mailto:"]' ) ) {
		//.. else if the element is a mailto link..
		beforeLabel = 'Email Address Clicked | ';
    
    } else if (element.is('form')) {
        //.. else if the element is a form..
        beforeLabel = 'Form Submissions | ';

	} else {
		beforeLabel = '';
	}

	return beforeLabel;
}
function gaGetLabel( element, args ) {
	var gaLabel;

	// Check and Set label property
	if( args && args.label ) {
		//.. If a label is passed in the args..
		gaLabel = args.label;

	} else if( element.data( 'label' ) ) {
		//.. else if the data-label property is set, use that as the labl
		gaLabel = element.data( 'label' );

	} else if( element.is( 'form' ) ) {
		//If the element is a form..

		gaLabel = 'Form';

		if( element.find('.ga-form input[type="hidden"]').val() ) {
			gaLabel = element.find('.ga-form input[type="hidden"]').val();
		}

	} else {
		gaLabel = element.text();
	}

	return gaLabel;
}
/*
 * This function is used to gather text 
 * that is dsiplayed after the label
 * Example: {File Name} | Downloaded
*/

function gaGetAfterLabel( element, args ) {

	var afterLabel;

	// If the afterLabel property is passed..
	if( args && args.afterLabel ) {
		afterLabel = args.afterLabel;

	} else if( element.data( 'after-label' ) ) {
		//.. else if the element has the data-after-label proprty set..
		afterLabel = element.data( 'after-label' );

	} else if( element.is( 'form' ) ) {
		//.. else if the element is a form..
		afterLabel = '';

	} else if( element.hasClass( 'ga-download' ) ) {
		//.. else if the element is a mailto link..
		afterLabel = ' | Downloaded';

	} else {
		afterLabel = '';
	}

	return afterLabel;
}
/*
 * This function is used to gather the Category
 * that is sent to Google Analytics.
*/

function gaGetCategory( element, args ) {
	var category;

	// Check and set the category Property
	if( args && args.category ) {
		//.. else if the category is passed..
		category = args.category; 

	}  else if( element.data( 'category' ) ) {
		//.. else the element has the data-category value set..
		category = element.data( 'category' );

	} else if( element.is( 'a[href^="tel:"]' ) || element.is( 'a[href^="mailto:"]' ) || element.is( 'form' ) ) {
		category = 'Contact';

	} else if( element.hasClass( 'ga-download' ) ) {
		//.. else if the element has the ga-download class..
		category = 'Downloads';

	} else {
		//.. otherwise set the category to engagement
		category = 'Engagement';
	}

	return category;
}
/*
 * This function is used to gather the Action
 * that is sent to Google Analytics.
*/

function gaGetAction( element, args ) {
	var action;

	if( args && args.action ) {
		action = args.action;

	} else if( element.data( 'action' ) ) {
		action = element.data( 'action' );

	} else if( element.is( 'form' ) ) {
		action = 'Submit';

	} else if( element.hasClass( 'ga-download' ) ) {
		action = 'Download';

	} else {
		action = 'Click';
	}

	return action;
}
jQuery.fn.extend({
	gaGetEvent: function( args ) {

		let beforeLabel = gaGetBeforeLabel( this, args );
		let label = gaGetLabel( this, args );
		let afterLabel = gaGetAfterLabel( this, args );

		let eventObject = {
			beforeLabel : beforeLabel,
			label : label,
			afterLabel : afterLabel,
			category : gaGetCategory( this, args ),
			action : gaGetAction( this, args ),
			eventLabel : beforeLabel + label + afterLabel
		};

		return eventObject;
	},
	gaSendEvent: function( trigger, args, debug ) {
		let gaEvent = this.gaGetEvent( args );
		let eventLabel = gaEvent.eventLabel;
		let eventCategory = gaEvent.category;
		let eventAction = gaEvent.action;

		this.on( trigger, function(e) {
			if( debug ) {
				e.preventDefault();
				console.log( gaEvent );
			} else {
				ga('send', 'event', eventCategory, eventAction, eventLabel );
			}
				
		} );			
	}	

}); 

//# sourceMappingURL=jquery.ga-lib.js.map