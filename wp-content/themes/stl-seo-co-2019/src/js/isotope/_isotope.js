//@prepros-prepend _isotope.pkgd.min.js

jQuery(document).ready(function ($) {
    
    $(function () {
        var $container = $('#case-study-filter');
    
        $container.isotope({})
    
        $('#filter-select-industry').change(function () {
            $container.isotope({
                filter: this.value
            });
        });
    });
});