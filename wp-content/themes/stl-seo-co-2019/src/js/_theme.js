// Animate on scroll
var scroll = window.requestAnimationFrame || function(callback) { window.setTimeout(callback, 1000 / 60) };
var elementsToShow = document.querySelectorAll('.u-animate-media');
//var borderToAnimate = document.querySelectorAll('.u-animate__border-top');
var animateFadeUp = document.querySelectorAll('.u-animate__fade-up');
var animateBorderTop = document.querySelectorAll('.u-animate__border-top');
var animateBorderBottom = document.querySelectorAll('.u-animate__border-bottom');
var animateServiceImages = document.querySelectorAll('.service__images');

function loop() {
    
    if ( elementsToShow.length > 0){
        elementsToShow.forEach(function (element) {
            if (isElementInViewport(element)) {
                element.classList.add('u-animate-fade');

            } else {
                element.classList.remove('u-animate-fade');
            }
        });
    }

    if (animateFadeUp.length > 0) {
        animateFadeUp.forEach(function (element) {
            if (isElementInViewport(element)) {
                element.classList.add('animate');

            } else {
                element.classList.remove('animate');
            }
        });
    }
    
    if (animateBorderTop.length > 0) {
        animateBorderTop.forEach(function (element) {
            if (isElementInViewport(element)) {
                element.classList.add('animate');

            } else {
                element.classList.remove('animate');
            }
        });
    }   

    if (animateBorderBottom.length > 0) {
        animateBorderBottom.forEach(function (element) {
            if (isElementInViewport(element)) {
                element.classList.add('animate');

            } else {
                element.classList.remove('animate');
            }
        });
    }  

    if (animateServiceImages.length > 0) {
        animateServiceImages.forEach(function (element) {
            if (isElementInViewport(element)) {
                element.classList.add('animate');
            } else {
                element.classList.remove('animate');
            }
        });
    }  
    scroll(loop);
}

// Call the loop for the first time
loop();

// Helper function from: http://stackoverflow.com/a/7557433/274826
function isElementInViewport(el) {
    // Special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }
    var rect = el.getBoundingClientRect();
    return (
        (rect.top <= 0 &&
            rect.bottom >= 0) ||
        (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.top <= (window.innerHeight || document.documentElement.clientHeight)) ||
        (rect.top >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
    );
}



$(document).ready(function() {

    // All Carousels
    var circle_carousels = $('.circle-module-carousel');
    var testimonial_carousels = $('.carousel_testimonial');

    testimonial_carousels.each(function() {
        $(this).owlCarousel({
            items: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            autoplayHoverPause: true,
            autoHeight: false,
            nav: true,
            navSpeed: 1000,
            mouseDrag: false
        });
    })

    circle_carousels.each(function() {
        $(this).owlCarousel({

            responsive: {
                0: {
                    items: 1,
                    nav: false,
                    dots: true,
                    margin: 1,
                    stagePadding: 10,
                    center: false,
                    autoWidth: false,
                    mouseDrag: true
                },
                576: {
                    items: 1,
                    nav: false,
                    dots: true
                },
                768: {
                    items: 4,
                    nav: false,
                    dots: true
                },
                1200: {
                    items: 5,
                    nav: false,
                    dots: false,
                    loop: false,
                    mouseDrag: false
                }
            }
        });
    });

    // Service Module
    var service_navigation = $('.service__navigation a');

    service_navigation.each(function() {
        $(this).click(function() {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
        })
    });

    //Make sure smooth scroll stop in top third of page.
    $.fn.inTopThird = function() {

        var viewportThird = $(window).height() / 3; //viewport top third
        var elementOffset = $(this).offset().top; //distace from top of screen
        var scrollDistance = $(window).scrollTop(); //distace element has scrolled
        var topThird = elementOffset - viewportThird;
        return scrollDistance > topThird; //is in top third
    };
 
    //Select all links with hashes for smooth scroll
    $('a[href*="#"]').on('click', function(e) {
        //e.preventDefault();
        // var navHeight = $(".drawer-navbar").height() + 125; //
        var target = $(this).attr('href');
        var $target = $(target);
        var targetOffset = $target.offset().top - 100; // navHeight

        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');

        $('html, body').animate({
            scrollTop: targetOffset
        }, 1000, 'linear');

    });

});