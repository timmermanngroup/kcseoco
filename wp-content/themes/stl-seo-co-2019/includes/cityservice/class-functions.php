<?php

namespace CITYSERVICE;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * This class contains all CITYSERVICE specific functions so that they are namespaced
 */
class Functions
{
    /**
     * Returns a string of term slugs of a post by taxonomy 
     * 
     * @param   int/string $postID
     * @param   string     $taxomony
     * 
     */
    public static function taxomony_term_chooser( $taxomony , $chooser_title){
        $terms = get_terms( array(
            'taxonomy' => $taxomony,
            'hide_empty' => false,
            ) );

        $term_chooser  = '<select id="filter-select-' . $taxomony . '" class="filter-select-' . $taxomony . '">';
        $term_chooser .= '<option value="*" selected="selected">'. ucfirst( $chooser_title ) .'</option>';
        foreach($terms as $term){
		    $term_chooser .= '<option value=".' . $term->slug . '">'. ucfirst( $term->name )  .'</option>';
	    }
	    $term_chooser .= '</select>';

	    return $term_chooser;
    }
    
    /**
    * 
    *	Returns a string of term slugs of a post by taxonomy 
    *	$postID 	= int or string
    *	$taxomony 	= string	
    *	
    */
    public static function taxomony_term_string( $postID , $taxomony ){
        
        $post_terms = get_the_terms( $postID, $taxomony );
        
        $term_list = null;
        foreach($post_terms as $term){
            $term_list .= $term->slug . ' ';
        }
        return $term_list;
    }


    /**
     * Return the Concatenated String
     * @access  public 
     * @static
     * @param   string   $string
     * @return  string
     */
    public static function get_concatenated_string( $string ) 
    {
        $string = strtolower( join('-', explode( ' ' , $string ) ) );
        
        return apply_filters( __METHOD__, $string ) ;    
    }


}