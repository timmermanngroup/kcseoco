<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Branding{


    /**
     * Return Header Logo
     * @access  public
     * @static
     * 
     * @param string $logo_url
     */
    public static function get_logo( $logo_url ) 
    {
       return apply_filters( __METHOD__ , $logo_url );
    }
    
    
    /**
     * Return Footer Logo
     * @access  public
     * @static
     * 
     * @param string $logo_url
     */
    public static function format_logo( $logo_url ) 
    {

        ob_start();
        
        if( mime_content_type( get_stylesheet_directory() . '' . $logo_url ) == 'image/svg+xml' ){
            echo file_get_contents( get_stylesheet_directory() . $logo_url );
        }else{
            echo '<img alt="' . get_bloginfo( 'name' ) . '" src="' . get_stylesheet_directory_uri() . $logo_url . '">';
        }
        
        return apply_filters( __METHOD__ , ob_get_clean() );
    }
    
    
    /**
	 * Renders Branding
     * @access  public
     * @static
     * 
     * @param string $home_url
     * @param string $logo
     * @param string $position
     * 
	 */  
    public static function get_markup( $logo_url = '' )
    {
        
        ob_start(); ?>
        
        <a class="branding" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <?php echo self::format_logo( self::get_logo( $logo_url ) ); ?>
            <span class="sr-only"><?php bloginfo( 'name' ); ?></span>
        </a>
            
        <?php
        return apply_filters( __METHOD__ , ob_get_clean() );
    }


}