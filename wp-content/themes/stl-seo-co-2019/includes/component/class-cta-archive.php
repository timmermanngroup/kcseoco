<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Cta_Archive extends Component implements Cta_Interface{


    const HEADING       =   'heading';
    const SUB_HEADING   =   'sub_heading';
    const BUTTON        =   'button';
    const IMAGE         =   'image';
    
    
    /**
     * Return CTA Title
     * @access  public
     * @static
     * @param   array   $component_data
     */
    public function get_heading( $component_data ) {

        return apply_filters( __METHOD__ , $component_data[self::HEADING] );
    }
    
    
    /**
     * Return CTA Sub Title
     * @access  public
     * @static
     * @param   array   $component_data
     */
    public function get_sub_heading( $component_data ) {

        return apply_filters( __METHOD__ , $component_data[self::SUB_HEADING] );
    }
    
    
    /**
     * Return CTA Button
     * @access  public
     * @static
     * @param   array   $component_data
     */
    public function get_button( $component_data ) {
        
        $button = \TG\Functions::get_button( $component_data[self::BUTTON] , array( 'class' => 'cta--button u-dropshadow--1') );
    
        return apply_filters( __METHOD__ , $button );
    }
    
    
    /**
     * Return CTA Image
     * @access  public
     * @static
     * @param   array   $component_data
     */
    public function get_image( $component_data ) {
        
        $image = wp_get_attachment_image( $component_data[self::IMAGE] , 'fullsize' , false , array('class' => 'img-circle' ) );
        
        return apply_filters( __METHOD__ , $image );
    }
    
    
    /**
	 * Renders CTA
	 * @var string
	 */  
    public function get_markup( $component_data ){
        
        $content  = $this->get_heading( $component_data ) ? '<h3 class="h1 cta--heading">' . $this->get_heading( $component_data ) . '</h3>' : null;
        $content .= $this->get_sub_heading( $component_data ) ? '<p class="h3 cta--subheading">' .  $this->get_sub_heading( $component_data ) . '</p>' : null;
        $button   = $this->get_button( $component_data );
        $image    = $this->get_image( $component_data ); 
        
        if( empty( $content ) ) {
            return;
        }

        ob_start(); ?>
        
        <div class="container">
            <div class="row d-flex justify-content-between">
                <div class="col-lg-6 cta--content">
                    <?php echo $content; ?>
                </div>
                <div class="col-md-6">    
                    <figure class="cta-media u-media-circle u-dropshadow--1 d-none d-lg-block "><?php echo $image; ?></figure>
                </div>
                <?php echo $button; ?>  
            </div>
        </div>
        
        <?php
        return apply_filters( __METHOD__ , ob_get_clean() );
    }


   /**
     * Output the Main Content
     * @access  public
     * @param   array       $module
     */
    public function component_content( $component_data ) {
        //echo $this->get_raw_data( $component_data );
        return $this->get_markup( $component_data );
    }

}