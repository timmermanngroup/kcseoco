<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Post_Meta
{


    /**
	 * Renders Updated Date
     * @access  public
     * @static
     * 
	 */  
    public static function get_modified_date()
    {
        
        ob_start();
        
        $time_string = '<time class="updated" datetime="%1$s">%2$s</time>';
        
        echo sprintf( $time_string,
		    esc_attr( get_the_modified_date( 'c' ) ),
		    esc_html( get_the_modified_date() )
	    );
        
        return apply_filters( __METHOD__ , ob_get_clean() );
    }
    
    
    /**
	 * Renders Published Date
     * @access  public
     * @static
     * 
	 */  
    public static function get_published_date()
    {
        
        ob_start();
        
        $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';

        echo sprintf( $time_string,
		    esc_attr( get_the_date( 'c' ) ),
		    esc_html( get_the_date() )
	    );
        return apply_filters( __METHOD__ , ob_get_clean() );
    }
    
    
    /**
	 * Renders Byline
     * @access  public
     * @static
     * 
	 */  
    public static function get_byline()
    {
        global $post;
        
        ob_start();
        echo 'By: <span class="author vcard">'. esc_html( get_the_author_meta('display_name', $post->post_author ) ) .'</span>';
        return apply_filters( __METHOD__ , ob_get_clean() );
    }



    /**
     * Echo the Post Meta based on layout
     * @access  public
     * @static
     * 
     * @param string $layout
     * 
     * @return  html
     */
    public static function the_post_meta( $layout = '' ) {
        
        echo '<div class="post--meta">';
        switch( $layout ){
            case 'byline':
                echo self::get_byline();
                break;

            case 'modified_date':
                echo self::get_modified_date();
                break;
            
            case 'combined_meta':
                echo self::get_published_date() . ' | ' . self::get_byline();
                break;
            
            case 'published':
                echo self::get_published_date();
                break;

            default:
                echo self::get_published_date();
        }
        echo '</div>';
    }

}