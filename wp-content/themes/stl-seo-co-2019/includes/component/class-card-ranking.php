<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//class Card_Ranking extends Component
class Card_Ranking
{
    
    const ID_PREFIX 	        = 	'component--';
    const KEYWORD_PHRASE 	    = 	'keyword_phrase';
    const KEYWORD_RANK 	        = 	'keyword_rank';
    const CLIENT_WEB_ADDRESS 	= 	'client_web_address';
    const VERIFY_LINK 	        = 	'';
    
    
    /**
	 * Display Client Name
	 * @access 	protected
     * @param   boolean $display_client;
	 * 
	 */
    protected $display_client;
    

    /**
	 * Display Client URL
	 * @access 	protected
     * @param   boolean $display_client_url;
	 * 
	 */
    protected $display_client_url;


    /**
	 * Display Keyword Phrase
	 * @access 	protected
     * @param   boolean $display_keyword_phrase;
	 * 
	 */
    protected $display_keyword_phrase;
    

    /**
	 * Display Keyword Rank
	 * @access 	protected
     * @param   boolean $display_keyword_rank;
	 * 
	 */
    protected $display_keyword_rank;
    
    
    /**
	 * Display Verify Button
	 * @access 	protected
     * @param   boolean $display_verify_button;
	 * 
	 */
    protected $display_verify_button;
    
    
    /**
	 * Component Data
	 * @access 	protected
     * @param   array $component_data;
	 * 
	 */
    protected $component_data;
    
    
    /**
	 * The main constructor method
	 * @access 	public
	 * @param  	array
	 * @return 	void
	 */
    public function __construct( $post_ID, $display_args = array() ) 
    {
        
        $args = wp_parse_args( $display_args  , $this->component_defaults() );
        
        foreach( $args as $key => $value ){
            if ( property_exists( $this , $key ) ){
                $this->$key = $value;
            }
        }
        $this->component_data =  get_post_meta( $post_ID );
        $this->content 	= 	$this->get_markup( $this->component_data );
        $this->ID       =   apply_filters( __METHOD__ , uniqid( self::ID_PREFIX ) );
    }
    
    
    /**
	 * Create the compenent defaults
	 * @access 	private
	 * @return 	html
	 */
    private function component_defaults() 
    {
        $component_defaults = array(
            'display_client'            => TRUE,
            'display_client_url'        => TRUE,
            'display_keyword_phrase'    => TRUE,
            'display_keyword_rank'      => TRUE,
            'display_verify_button'     => TRUE,
        );

        return $component_defaults;
	}	
    
    
    /**
	 * Create the opening element
	 * @access 	private
	 * @return 	html
	 */
    private function component_start() 
    {
        return '<div id="' . $this->ID . '" class="component__content component--card-ranking">';
	}	
    
    
    /**
	 * Create the closing element
	 * @access 	private
	 * @return 	html
	 */
    private function component_end() 
    {
		return '</div>';
    }


    /**
	 * Get Keyword Phrase
	 * @access 	private
	 * @return 	string 
	 */
    private function get_keyword_phrase( $component_data ) 
    {
        if ( ( $this->display_keyword_phrase != TRUE ) || empty( $component_data[self::KEYWORD_PHRASE][0] )){
            return apply_filters( __METHOD__, __return_false() );
        }
        
        return apply_filters( __METHOD__, $component_data[self::KEYWORD_PHRASE][0] );  
	}	

    
    
    
    
    private function get_rank_color( $component_data ) 
    {
        if ( ( $this->display_keyword_rank != TRUE ) || empty( $component_data[self::KEYWORD_RANK][0] )){
            return apply_filters( __METHOD__, __return_false() );
        }

        //Get card color based on rank
        switch ( $component_data[self::KEYWORD_RANK][0] ) {
            case '5':
                $card__color  = 'portfolio-color__5';
                break;
             case '4':
                $card__color  = 'portfolio-color__4';
                break;
            case '3':
                $card__color  = 'portfolio-color__3';
                break;                
             case '2':
                $card__color  = 'portfolio-color__2';
                break;
            case '1':
                $card__color  = 'portfolio-color__1';
                break;
            default: 
                $card__color  = 'portfolio-color__default';
        }
        return apply_filters( __METHOD__, $card__color );  
    }	



    /**
	 * Get Keyword Rank
	 * @access 	private
	 * @return 	string 
	 */
    private function get_keyword_rank( $component_data ) 
    {
        if ( ( $this->display_keyword_rank != TRUE ) || empty( $component_data[self::KEYWORD_RANK][0] )){
            return apply_filters( __METHOD__, __return_false() );
        }

        return apply_filters( __METHOD__, $component_data[self::KEYWORD_RANK][0] );  
    }	
    
    /**
	 * Get Client Web Address
	 * @access 	private
	 * @return 	string 
	 */
    private function get_client_name( $component_data ) 
    {
        if ( ( $this->display_client != TRUE ) || empty( get_post_ancestors( get_the_ID() )[0] )){
            return apply_filters( __METHOD__, __return_false() );
        }
        
        $client_name = \TG\Functions::superscript_filter( get_the_title( get_post_ancestors( get_the_ID() )[0] ) );
        return apply_filters( __METHOD__, $client_name );  
	}

    /**
	 * Get Client Web Address
	 * @access 	private
	 * @return 	string 
	 */
    private function get_client_web( $component_data ) 
    {
        if ( ( $this->display_client_url != TRUE ) || empty( $component_data[self::CLIENT_WEB_ADDRESS][0] )){
            return apply_filters( __METHOD__, __return_false() );
        }
        return apply_filters( __METHOD__, $component_data[self::CLIENT_WEB_ADDRESS][0] );  
	}	


    /**
	 * Build Search Query
	 * @access 	private
	 * @return 	string 
	 */
    private function build_search_query( $component_data ) 
    {
        if(  empty( $component_data[self::KEYWORD_PHRASE][0] )){
            return apply_filters( __METHOD__, __return_false() );
        }
        
        $search_query_string = '//www.google.com/search?q=' . urlencode( $component_data[self::KEYWORD_PHRASE][0] );
        
        return apply_filters( __METHOD__, $search_query_string ); 
    }	
    
    
    /**
	 * Render Verify Button
	 * @access 	private
	 * @return 	string 
	 */
    private function render_verify_button( $component_data ) 
    {
        if ( ( $this->display_verify_button != TRUE ) || empty( $this->build_search_query( $component_data ) )){
            return apply_filters( __METHOD__, __return_false() );
        }

        ob_start();
        echo '<a class="btn btn--color-white button--pill" href="' . $this->build_search_query( $component_data ) . '" target="_blank">Verify The Ranking</a>';

        return apply_filters( __METHOD__, ob_get_clean()); 
    }	
    
    
    /**
	 * Renders Card
	 * @var string
	 */  
    public function get_markup( $component_data )
    {
        $rank_color         =  $this->get_rank_color( $this->component_data ) ?: null;     
        $keyword_phrase     =  $this->get_keyword_phrase( $this->component_data ) ?: null;
        $keyword_rank       =  $this->get_keyword_rank( $this->component_data ) ?: null;
        $client_name        =  $this->get_client_name( $this->component_data ) ?: null;
        $client_web_address =  $this->get_client_web( $this->component_data ) ?: null;
        $verify_button	    =  $this->render_verify_button( $this->component_data ) ?: null;

        
        ob_start();?>
        
        <article id="post-<?php the_ID(); ?>" <?php post_class( 'portfolio-card ' ); ?>>
            <header class="card-header">
                <?php 
                    echo '<span class="card--client">' . $client_name   . '</span>';
                    echo '<span class="card--web-adress">' . $client_web_address . '</span>';
                    ?>
            </header>
            <div class="card-content">
                <?php 
                    echo '<span class="card--ranking"><span class="rank">#' . $keyword_rank . '</span> Google US Ranking:</span>';
                    echo '<span class="card--keywords">' . $keyword_phrase   . '</span>';
                    ?>
            </div>
            <footer class="card-footer">
                <?php 
                    echo $verify_button;
                    ?>
            </footer>
        </article>

        <?php
        return apply_filters( __CLASS__.'/'.__FUNCTION__, ob_get_clean() );
    }
    








    
    /**
	 * Output the formatted HTML
	 * @access 	public
	 * @return 	html
	 */
    public function output() 
    {
		$content 	 =	$this->component_start();
        $content 	.=	$this->content;
		$content 	.=	$this->component_end();

		return $content;
    }
}