<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Card_Case_Study extends Component implements Card_Interface
{

    const CARD_TITLE = 'details_company_name';
    const CARD_INTRO = 'details_introduction';
    const CARD_IMAGE = 'background_image';
    
    /**
     * Return the Case Study Title
     * @access  private 
     * @static
     * @param   array[mixed]   $post_meta
     * @return  string
     */
    private static function get_card_title( $post_meta ){
        if (isset( $post_meta[self::CARD_TITLE][0] ) && !empty( $post_meta[self::CARD_TITLE][0] )) {
            return apply_filters(__METHOD__, $post_meta[self::CARD_TITLE][0] );
        }
    }

    /**
     * Return the Case Study Intro
     * @access  private 
     * @static
     * @param   array[mixed]   $post_meta
     * @return  string
     */
    private static function get_card_intro( $post_meta ){
        if (isset( $post_meta[self::CARD_INTRO][0] ) && !empty( $post_meta[self::CARD_INTRO][0] )) {
            return apply_filters(__METHOD__, $post_meta[self::CARD_INTRO][0] );
        }
    }

    /**
     * Return the Case Study Image
     * @access  private 
     * @static
     * @param   array[mixed]   $post_meta
     * @return  string
     */
    private static function get_card_image( $post_meta ){
        
        if (isset( $post_meta[self::CARD_IMAGE][0] ) && !empty( $post_meta[self::CARD_IMAGE][0] )) {
            $card_image = wp_get_attachment_image( $post_meta[self::CARD_IMAGE][0] , 'card', false );
        }else{
            $card_image = '<img alt="stl seo" src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/branding/card-default.png" />';
        }

        return apply_filters(__METHOD__, $card_image );
    }

    /**
	 * Renders Card
	 * @var string
	 */  
    public function get_markup( $component_data ){
        
        $post_id        = get_the_ID();
        $card_title     = self::get_card_title( get_post_meta( $post_id ) ) ? '<h3 class="card--title">'. self::get_card_title( get_post_meta( $post_id ) ).'</h3>' : null; 
        $card_intro     = self::get_card_intro( get_post_meta( $post_id ) ) ? self::get_card_intro( get_post_meta( $post_id ) ) : null;
        $card_image     = self::get_card_image( get_post_meta( $post_id ) );
        //$card_image     = wp_get_attachment_image( $card_image_id, 'card', false );

        ob_start();?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('u-card-outlined'); ?>> 
                <header class="card-header">
                    <?php echo \TG\Functions::superscript_filter( $card_title ) ; ?>
                </header>
                <figure>
                    <?php echo $card_image; ?>
                </figure>
                <div class="card-content">
                     <?php echo $card_intro; ?>
                </div>
                <footer>
                    <a class="btn btn__icon btn__r_arrow" href="<?php the_permalink();?>">Read More</a>
                </footer>
                
            </article>

        <?php
        return apply_filters( __CLASS__.'/'.__FUNCTION__, ob_get_clean() );
    }

}