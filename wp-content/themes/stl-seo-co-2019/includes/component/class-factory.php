<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Factory {


	/**
	 * Build method for creating instances of Classes
	 * @access 	public
	 * @static
	 * @param 	array 	$component_data
	 * @return 	void
	 */
	public static function build( $type = '' ,  $component_data = array() ) {
        
        if( empty( $component_data ) ) {
			throw new \Exception( 'Empty Component Content' );			

		} else {
            //$class_name 	= 	'Component_' . self::get_class_name( $type );  //Component_Hero_Page
            $class_name 	= 	'Component\\' . self::get_class_name( $type );  //Component_Hero_Page

			if( class_exists( $class_name ) ) {
                $object =  new $class_name( $type, $component_data );
                echo $object->output();
            
            } else {
				throw new \Exception( "<strong>$class_name</strong> not found" );				
			}
		}
    }

	/**
	 * Make the class name
	 * @access 	private
	 * @static
	 * @param 	string 		$type
	 * @return 	string
	 */
	private static function get_class_name( $type = '' ) {
		if( $type != '' ) {
			$type 	= 	str_replace( array( '-', '_' ), ' ', strtolower( $type ) );
			$type 	= 	explode( ' ' , $type );

			foreach( $type as $string ) {
				$keys[] 	=	ucfirst( $string );
			}

			$type 	= 	join( '_', $keys );
		}
        return $type;
	}


}