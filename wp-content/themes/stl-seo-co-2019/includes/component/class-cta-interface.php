<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

interface Cta_Interface{
    
    /**
	 * Get CTA Heading
     * @param   array $component_data
	 */
    public function get_heading( $component_data );
    
    /**
	 * Get CTA Sub Heading
     * @param   array $component_data
	 */
    public function get_sub_heading( $component_data );

    /**
     * Output the CTA Button
     * @access  public
     * @param   array $component_data
     */
    public function get_button( $component_data );

    /**
	 * Get CTA Image
     * @param   array $component_data
	 */
    public function get_image($component_data );

    /**
	 * Renders CTA
     * @param   array $component_data
	 */  
    public function get_markup( $component_data );

    /**
     * Output the CTA Content
     * @access  public
     * @param   array $component_data
     */
    public function component_content( $component_data );

}