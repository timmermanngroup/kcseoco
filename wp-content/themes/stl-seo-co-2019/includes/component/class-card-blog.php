<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Card_Blog extends Component implements Card_Interface
{   
    
    /**
     * Return the Blog Featured Image
     * @access  private 
     * @static
     * @param   int   $post_id
     * @return  string
     */
    private static function get_card_image( $post_id ){
        
        if ( has_post_thumbnail() ) {
            $card_image = get_the_post_thumbnail( $post_id , 'card' );
        }else{
            $card_image =  '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/branding/card-default.png" />';
        }
 
        return apply_filters(__METHOD__, $card_image );
    }

    /**
	 * Renders Card
	 * @var string
	 */  
    public function get_markup( $component_data ){
        
        ob_start();?>
        
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <a href="<?php the_permalink();?>" class="u-card-outlined">
                    <header class="card-header">
                        <?php 
                            \Component\Post_Meta::the_post_meta('publish_date');
                            $post_title =  \TG\Functions::superscript_filter( get_the_title() );   
                            echo '<h3 class="card--title u-animate__border-bottom">' . $post_title . '</h3>';
                            ?>
                    </header>
                    <figure class="u-media-circle">
                        <?php echo self::get_card_image( get_the_ID() ); ?>
                    </figure>
                    <div class="card-content">
                        <?php the_excerpt() ?>
                    </div>
                    <footer>
                        <?php \Component\Post_Meta::the_post_meta('byline');?>
                    </footer>
                </a>
            </article>

        <?php
        return apply_filters( __CLASS__.'/'.__FUNCTION__, ob_get_clean() );
    }

}