<?php

namespace Component;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

abstract class Component{

    const ID_PREFIX 	= 	'component--';

	/**
	 * The ID for this instance of the class
	 * @access 	public
	 * @var 	string
	 */
	public $ID;
    
    
	/**
	 * The component type specified when component is called
	 * @access 	public
	 * @var 	string
	 */
	public $type 	= 	'';
    
    /**
	 * The array of class names
	 * @access 	public
	 * @var 	array
	 */
	public $classes = ['component__content'];	

	/**
	 * The content for the Class
	 * @access 	public
	 * @var 	string
	 */
	public $content = '';


    /**
	 * The main constructor method
	 * @access 	public
	 * @param  	array
	 * @return 	void
	 */
	public function __construct( $type, $component_data ) {
		$this->type 	 = 	strtolower( $type );
		$this->content 	 = 	$this->component_content( $component_data );
		$this->classes[] = 	self::ID_PREFIX . str_replace( [' ', '_'], '-', $this->type );
        $this->ID 		 = 	apply_filters( __CLASS__.'/'.__FUNCTION__.'/ID', uniqid( self::ID_PREFIX ) );
	}
    
    
    /**
	 * Create the opening element
	 * @access 	private
	 * @return 	html
	 */
	private function component_start() {

		return '<div id="' . $this->ID . '" class="' . join( ' ', $this->classes ) . '">';
	}	
    
    
    /**
	 * Create the closing element
	 * @access 	private
	 * @return 	html
	 */
	private function component_end() {
		return '</div>';
    }
    
    

    /**
	 * Return the content
	 * @access 		public
	 * @abstract
	 * @return 		void
	 */
    public function component_content( $component_data ) {
        //echo    $this->get_raw_data( $component_data );
        return  $this->get_markup( $component_data );
    }
    
    
    /**
	 * Output the formatted HTML
	 * @access 	public
	 * @return 	html
	 */
	public function output() {
		$content 	=	$this->component_start();
		$content 	.=	$this->content;
		$content 	.=	$this->component_end();

		return $content;
    }
    
    /**
	 * Return Raw ACF Data
	 * @access 	protected
	 * @param 	array 		$module
	 */
	protected function get_raw_data( $component_data ) {
		ob_start();
		
		echo '<pre>';
		print_r( $component_data );
		echo '</pre>';

		return ob_get_clean();
	}


}

