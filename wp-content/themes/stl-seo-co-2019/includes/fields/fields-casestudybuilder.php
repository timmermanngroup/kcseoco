<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5d20b9a328fc0',
        'title' => 'Case Study Builder',
        'fields' => array(
            array(
                'key' => 'field_5d20ba99e9b5e',
                'label' => 'Case Study Builder',
                'name' => 'case_study_builder',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layouts' => array(
                    '5d20bdd00d0c9' => array(
                        'key' => '5d20bdd00d0c9',
                        'name' => 'page_introduction',
                        'label' => 'Case Study Introduction',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d20c44cb7d0f',
                                'label' => 'Case Study Introduction Fields',
                                'name' => 'case_study_introduction_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5cf7c065925df',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d21f5f800cdc' => array(
                        'key' => 'layout_5d21f5f800cdc',
                        'name' => 'content_section',
                        'label' => 'Content Section',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d21f5f800cdd',
                                'label' => 'Content Section Fields',
                                'name' => 'content_section_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d1de9262e372',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d21f7852e09f' => array(
                        'key' => 'layout_5d21f7852e09f',
                        'name' => 'case_study_portfolio',
                        'label' => 'Case Study Portfolio',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d21f7852e0a0',
                                'label' => 'Case Study Portfolio Fields',
                                'name' => 'case_study_portfolio_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d21f76a06863',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d21f721e0109' => array(
                        'key' => 'layout_5d21f721e0109',
                        'name' => 'case_study_testimonial',
                        'label' => 'Case Study Testimonial',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d21f721e010a',
                                'label' => 'Case Study Testimonial Fields',
                                'name' => 'case_study_testimonial_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d21f6eb028b9',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d2348f1fe613' => array(
                        'key' => 'layout_5d2348f1fe613',
                        'name' => 'related_case_studies',
                        'label' => 'Related Case Studies',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d2348f1fe614',
                                'label' => 'Related Case Study Fields',
                                'name' => 'related_case_study_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d234a3063f7d',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d21f7bed6614' => array(
                        'key' => 'layout_5d21f7bed6614',
                        'name' => 'cta',
                        'label' => 'Call To Action',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d21f7bed6615',
                                'label' => 'Call To Action Fields',
                                'name' => 'call_to_action_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5c1d53f590ec8',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                ),
                'button_label' => 'Add Row',
                'min' => '',
                'max' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_template',
                    'operator' => '==',
                    'value' => 'page-templates/case-study-builder.php',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'acf_after_title',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'the_content',
        ),
        'active' => true,
        'description' => '',
    ));

endif;