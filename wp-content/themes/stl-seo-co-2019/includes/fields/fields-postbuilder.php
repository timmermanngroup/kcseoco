<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5d1ddcfeaa536',
        'title' => 'Post Builder',
        'fields' => array(
            array(
                'key' => 'field_5d1ddd2291b3e',
                'label' => 'Post Builder',
                'name' => 'post_builder',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layouts' => array(
                    '5d1ddd3a821d6' => array(
                        'key' => '5d1ddd3a821d6',
                        'name' => 'page_introduction',
                        'label' => 'Post Introduction',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d20954a2a222',
                                'label' => 'Post Introduction Fields',
                                'name' => 'post_introduction_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5cf7c065925df',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d1deb5eb88c2' => array(
                        'key' => 'layout_5d1deb5eb88c2',
                        'name' => 'content_section',
                        'label' => 'Content Section',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d1deb5eb88c3',
                                'label' => 'Content Section Fields',
                                'name' => 'content_section_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d1de9262e372',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d1dea643049b' => array(
                        'key' => 'layout_5d1dea643049b',
                        'name' => 'blog',
                        'label' => 'Blog',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d1dea643049c',
                                'label' => 'Blog Fields',
                                'name' => 'blog_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d09357c41560',
                                ),
                                'display' => 'seamless',
                                'layout' => '',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d209586e91f8' => array(
                        'key' => 'layout_5d209586e91f8',
                        'name' => 'cta',
                        'label' => 'Call To Action',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d209586e91f9',
                                'label' => 'Call To Action Fields',
                                'name' => 'call_to_action_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5c1d53f590ec8',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                ),
                'button_label' => 'Add Row',
                'min' => '',
                'max' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_template',
                    'operator' => '==',
                    'value' => 'page-templates/post-builder.php',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'the_content',
        ),
        'active' => true,
        'description' => '',
    ));

endif;