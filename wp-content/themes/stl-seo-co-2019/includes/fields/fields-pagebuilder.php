<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_5c269fcbe6d25',
        'title' => 'Page Builder',
        'fields' => array(
            array(
                'key' => 'field_5c269fd5e695a',
                'label' => 'Page Builder',
                'name' => 'page_builder',
                'type' => 'flexible_content',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layouts' => array(
                    'layout_5ce842fba08a8' => array(
                        'key' => 'layout_5ce842fba08a8',
                        'name' => 'hero',
                        'label' => 'Hero',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5ce84331a08a9',
                                'label' => 'Hero Fields',
                                'name' => 'hero_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5ce83f319c377',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    '5c269ff5b7bf5' => array(
                        'key' => '5c269ff5b7bf5',
                        'name' => 'basic_content',
                        'label' => 'Basic Content',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5c26a3e483c50',
                                'label' => 'Basic Content Fields',
                                'name' => 'basic_content_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5c26a3a051afc',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5cf6dc5cd7058' => array(
                        'key' => 'layout_5cf6dc5cd7058',
                        'name' => 'page_introduction',
                        'label' => 'Page Introduction',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5cf7c123ddd68',
                                'label' => 'Page Introduction Fields',
                                'name' => 'page_introduction_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5cf7c065925df',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5cfad8f11ed27' => array(
                        'key' => 'layout_5cfad8f11ed27',
                        'name' => 'testimonials',
                        'label' => 'Testimonials',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5cfad8f11ed28',
                                'label' => 'Testimonial Fields',
                                'name' => 'testimonial_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5cfad826d7cb4',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5c707403e5953' => array(
                        'key' => 'layout_5c707403e5953',
                        'name' => 'image_banner',
                        'label' => 'Image Banner',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5c707403e5954',
                                'label' => 'Image Banner Fields',
                                'name' => 'image_banner_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5cf93b5b6674f',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d122e2bb18e5' => array(
                        'key' => 'layout_5d122e2bb18e5',
                        'name' => 'ranking_highlight',
                        'label' => 'Ranking Highlight',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d122e4a11a91',
                                'label' => 'Ranking Highlight Fields',
                                'name' => 'ranking_highlight_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d0d218e7c66d',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d1290c074029' => array(
                        'key' => 'layout_5d1290c074029',
                        'name' => 'portfolio',
                        'label' => 'Portfolio',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d1290c07402a',
                                'label' => 'Portfolio Fields',
                                'name' => 'portfolio_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d129061215b6',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5cf93c460e995' => array(
                        'key' => 'layout_5cf93c460e995',
                        'name' => 'blog',
                        'label' => 'Blog',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5cf93c460e996',
                                'label' => 'Blog Fields',
                                'name' => 'blog_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d09357c41560',
                                ),
                                'display' => 'seamless',
                                'layout' => '',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d0936560db88' => array(
                        'key' => 'layout_5d0936560db88',
                        'name' => 'cta',
                        'label' => 'Call To Action',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d0936560db89',
                                'label' => 'Call To Action Fields',
                                'name' => 'call_to_action_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5c1d53f590ec8',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d1b71aaa2640' => array(
                        'key' => 'layout_5d1b71aaa2640',
                        'name' => 'circle_content_carousel',
                        'label' => 'Circle Content Carousel ',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d1b71aaa2641',
                                'label' => 'Circle Content Carousel Fields',
                                'name' => 'circle_content_carousel_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d1b69cd9704f',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                    'layout_5d309b204d3ab' => array(
                        'key' => 'layout_5d309b204d3ab',
                        'name' => 'tabbed_services_section',
                        'label' => 'Tabbed Services Section',
                        'display' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5d309b204d3ac',
                                'label' => 'Tabbed Services Section Fields',
                                'name' => 'tabbed_services_section_fields',
                                'type' => 'clone',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'clone' => array(
                                    0 => 'group_5d3098981a405',
                                ),
                                'display' => 'seamless',
                                'layout' => 'block',
                                'prefix_label' => 0,
                                'prefix_name' => 0,
                            ),
                        ),
                        'min' => '',
                        'max' => '',
                    ),
                ),
                'button_label' => 'Add Row',
                'min' => '',
                'max' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => 'page-templates/page-builder.php',
                ),
            ),
        ),
        'menu_order' => -10,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'the_content',
        ),
        'active' => true,
        'description' => '',
    ));

endif;