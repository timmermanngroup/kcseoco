<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Hooks CTA module used at base of CTA archive pages.
 */
function cityservice_archive_cta() 
{
    
    if ( is_post_type_archive('case-study')  ){
        $cta_data = get_field('case_study_calls_to_action', 'options');

    }

    if ( empty( $cta_data )){
        return;
    }

    foreach( $cta_data as $cta){
        $cta['acf_fc_layout'] = 'cta';
        $cta = new Module\Cta( $cta );
        echo $cta->get_html( $cta);
    }

}  
add_action( \TG\Filters::MAIN_AFTER , 'cityservice_archive_cta');


/**
 * Hooks CTA module used at base of Blog archive pages.
 */
function cityservice_archive_cta_blog() 
{
    
    if ( is_home() || is_post_type_archive('post') || is_category() || is_tag() ){
        $cta_data = get_field('blog_cta', 'options');
    }


    if ( empty( $cta_data )){
        return;
    }
    $cta_data['acf_fc_layout'] = 'cta';
    $cta = new Module\Cta( $cta_data );
    echo $cta->get_html( $cta_data );
}  
add_action( \TG\Filters::MAIN_AFTER , 'cityservice_archive_cta_blog');

/**
 * Adds mobile navbar to emulate drawer sliding out
 */
function cityservice_mobile_before_main()
{ ?>

    <div class="drawer-navbar-header">
        <div class="container">
            <a class="branding" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <?php echo file_get_contents( get_stylesheet_directory_uri() . '/images/branding/stl-seo-logo.svg'); ?>
                <span class="sr-only"><?php bloginfo( 'name' ); ?></span>
			</a>
			<button type="button" class="drawer-toggle drawer-hamburger">
		      <span class="sr-only">toggle navigation</span>
		      <span class="drawer-hamburger-icon"></span>
		    </button>
        </div>
    </div>

<?php
}
//add_action( \TG\Filters::MAIN_BEFORE , 'cityservice_mobile_before_main' );


/**
 * Hook in Case Studies Hero
 */
function cityservice_blog_hero() 
{
    if ( is_home() ) {
        if ( function_exists( 'get_field' ) ){
            $hero = get_field('blog_hero', 'options');
        }
        $hero['acf_fc_layout'] = 'hero';

    }elseif ( is_category() ) {
        if ( function_exists( 'get_field' ) ){
            $background_image = get_field('category_image', get_queried_object() );
        }
        $hero = array(
            'heading'           => single_cat_title( 'Category: ', false ),
            'subheading'        => category_description(),
            'background_image'  => $background_image,
            'acf_fc_layout'     => 'hero-blog-single'
        );
    
    }elseif ( is_tag() ) {
        if ( function_exists( 'get_field' ) ){
            $background_image = get_field('tag_image', get_queried_object() );
        }
        $hero = array(
            'heading'           => single_tag_title( 'Tag: ', false ),
            'subheading'        => tag_description(),
            'background_image'  => $background_image,
            'acf_fc_layout'     => 'hero-blog-single'
        );
    
    }
    
    if ( empty( $hero )){
        return;
    }
    
    $hero = new \Module\Hero( $hero );
    echo $hero -> build_html();
}
add_action( \TG\Filters::MAIN_BEFORE, 'cityservice_blog_hero' , 10 );


/**
 * Hook in Blog Hero
 */
function cityservice_blog_single_hero() 
{
    if ( is_singular('post') ) {

        $background_image = '';
        
        if ( has_post_thumbnail( get_the_ID() ) ) {
            $background_image = get_post_thumbnail_id( get_the_ID() );
        }
        $hero = array(
            'heading'           => get_the_title(),
            'background_image'  => $background_image,
        );
    }
    
    if ( empty( $hero )){
        return;
    }
    
    $hero['acf_fc_layout'] = 'hero_blog_single';
    $hero = new \Module\Hero_Blog_Single( $hero );
    echo $hero -> build_html();
}
add_action( \TG\Filters::MAIN_BEFORE, 'cityservice_blog_single_hero' , 10 );


/**
 * Hook in Case Studies Hero
 */
function cityservice_case_studies_hero() {
    
    global $post;

    
    if ( is_post_type_archive('case-study') ) {
         
        if ( function_exists( 'get_field' ) ){
            $hero = get_field('case_study_hero', 'options');
        }   

    }elseif ( is_singular('case-study') ) {

        //The single case study
        if ( function_exists( 'get_field' ) ){
            $background_image = get_field('background_image');
        }

        $hero = array(
            'heading'           => get_the_title(),
            'background_image'  => $background_image,
        );

    }



    if ( empty( $hero )){
        return;
    }
    
    $hero['acf_fc_layout'] = 'hero';
    $hero = new \Module\Hero( $hero);
    echo $hero -> build_html();

}
add_action( \TG\Filters::MAIN_BEFORE, 'cityservice_case_studies_hero' , 10 );


/**
 * Hooks HERO module into the header of the contact page template.
 */
function cityservice_contact_template_hero() {
    
    if ( ! is_page_template('page-templates/page-contact.php')){
        return __return_false();
    }
       
    if ( function_exists( 'get_field' ) ){
        $hero = get_field('hero_fields');
    }
    
    if ( empty( $hero )){
        return __return_false();
    }
    
    $hero['acf_fc_layout'] = 'hero';
    $hero = new \Module\Hero( $hero );
    echo $hero -> build_html();
}  
add_action( \TG\Filters::MAIN_BEFORE , 'cityservice_contact_template_hero' , 10);


/**
 * Hook in Case Studies Introduction Module
 */
function cityservice_case_studies_introduction() 
{
    
    global $post;

    if ( is_post_type_archive('case-study') ) {
        if ( function_exists( 'get_field' ) ){
            $page_introduction = get_field('case_study_introduction', 'options');
        }   
    }
    
    if ( empty( $page_introduction  )){
        return;
    }
    
    $page_introduction ['acf_fc_layout'] = 'page-introduction';
    $page_introduction = new \Module\Page_Introduction( $page_introduction  );
    echo $page_introduction -> build_html();

}
add_action( \TG\Filters::MAIN_BEFORE, 'cityservice_case_studies_introduction' , 10 );

/**
 * Hook in Blog Introduction Module
 */
function cityservice_blog_introduction() 
{
    
    global $post;

    if ( is_home('case-study') ) {
        if ( function_exists( 'get_field' ) ){
            $page_introduction = get_field('blog_introduction', 'options');
        }   
    }
    
    if ( empty( $page_introduction  )){
        return;
    }
    
    $page_introduction ['acf_fc_layout'] = 'page-introduction';
    $page_introduction = new \Module\Page_Introduction( $page_introduction  );
    echo $page_introduction -> build_html();

}
add_action( \TG\Filters::MAIN_BEFORE, 'cityservice_blog_introduction' , 10 );


/**
 * Hooks start of Page Wrapper into the main page template.
 */
function cityservice_page_wrapper()
{
    // if ( is_page_template('page-templates/page-contact.php')){
    //     return;
    // }
    // if ( is_page_template('page-templates/page-builder.php')){
    //     return;
    // }
    // if ( is_page_template('page-templates/post-builder.php')){
    //     return;
    // }
    // if ( is_page_template('page-templates/case-study-builder.php')){
    //     return;
    // }
    // if ( is_singular('post')){
    //     return;
    // }
    // if ( is_singular('case-study')){
    //     return;
    // }
    if ( ! is_home()){
        return;
    }
    // if ( is_post_type_archive('case-study')){
    //     return;
    // }

    echo '<div class="blog-wrapper container-xl"><div class="row align-items-stretch">';
}
add_action( \TG\Filters::MAIN_LOOP_BEFORE , 'cityservice_page_wrapper' );


/**
 * Hooks close of Page Wrapper into the main page template.
 */
function cityservice_close_page_wrapper()
{
    // if ( is_page_template('page-templates/page-contact.php')){
    //     return;
    // }
    // if ( is_page_template('page-templates/page-builder.php')){
    //     return;
    // }
    // if ( is_page_template('page-templates/post-builder.php')){
    //     return;
    // }
    // if ( is_page_template('page-templates/case-study-builder.php')){
    //     return;
    // }
    // if ( is_singular('post')){
    //     return;
    // }
    // if ( is_singular('case-study')){
    //     return;
    // }
    if ( ! is_home()){
        return;
    }
    // if ( is_post_type_archive('case-study')){
    //     return;
    // }

    echo '</div></div>';
}
add_action( \TG\Filters::MAIN_LOOP_AFTER, 'cityservice_close_page_wrapper' );





/**
 * Hooks start of Page Wrapper into the Case Study archive template.
 */
function cityservice_case_studies_wrapper()
{
    if ( ! is_post_type_archive('case-study')){
        return;
    }
    
    echo '<div class="container-xl"><div class="row"><div class="col-12">';
    echo \CITYSERVICE\Functions::taxomony_term_chooser( 'industry' , 'All Services');
    echo '<div id="case-study-filter" class="case-study-filter">';
}
add_action( \TG\Filters::MAIN_LOOP_BEFORE , 'cityservice_case_studies_wrapper' );


/**
 * Hooks close of Page Wrapper into the Case Study archive template.
 */
function cityservice_close_case_studies_wrapper()
{
    if ( ! is_post_type_archive('case-study')){
        return;
    }

    echo '</div></div></div></div>';
}
add_action( \TG\Filters::MAIN_LOOP_AFTER, 'cityservice_close_case_studies_wrapper' );


/**
 * Hooks start of Page Wrapper into the Case Study single template.
 */
function cityservice_case_study_wrapper()
{
    
    if ( ! is_singular('case-study')){
       return;
    }
    if ( is_page_template('page-templates/case-study-builder.php')){
        return;
    }
    
    echo '<div class="container-xl"><div class="row"><div class="col-12">';
    
}
add_action( \TG\Filters::MAIN_LOOP_BEFORE , 'cityservice_case_study_wrapper' );


/**
 * Hooks close of Page Wrapper into the Case Study single template.
 */
function cityservice_close_case_study_wrapper()
{
    
    if ( ! is_singular('case-study')){
       return;
    }
    if ( is_page_template('page-templates/case-study-builder.php')){
        return;
    }
    

    echo '</div></div></div></div>';
}
add_action( \TG\Filters::MAIN_LOOP_AFTER, 'cityservice_close_case_study_wrapper' );


/**
 * Hooks start of Page Wrapper into the contact page template.
 */
function cityservice_contact_page_wrapper() 
{
    if ( ! is_page_template('page-templates/page-contact.php')){
        return;
    }

    echo '<div class="container-xl"><div class="row align-items-stretch"><div class="col-12 col-xs-12 col-lg-8">';
}  
add_action( \TG\Filters::MAIN_LOOP_BEFORE , 'cityservice_contact_page_wrapper' , 10);


/**
 * Hooks close of Page Wrapper into the contact page template.
 */
function cityservice_close_contact_page_wrapper() 
{
    if ( ! is_page_template('page-templates/page-contact.php')){
        return;
    }

    echo '</div></div></div>';
}  
add_action( \TG\Filters::MAIN_LOOP_AFTER , 'cityservice_close_contact_page_wrapper' , 10);


/**
 * Hooks the sidebar into the contact page template.
 */
function cityservice_sidebar_contact_page_template() 
{
    
    if ( ! is_page_template('page-templates/page-contact.php')){
        return;
    }

    if ( function_exists( 'get_field' ) ){
        $sidebar_options = get_field('sidebar_options');
        $sidebar_content = get_field('sidebar_content');
    }
    $contact_info           = \TG\Business_Info::get_contact_info();
    $contact_map            = wp_get_attachment_image( $sidebar_options['map'], 'full', false, array( "class"=>"img-circle"));
    $contact_link           = $sidebar_options['directions_link'] ?: null ; 
    $contact_link_target    = $contact_link ['target'] ? 'target="'. $contact_link ['target'] .'"' : null ; 

    
    echo '</div>';
    echo '<div class="col-12 col-xs-12 col-md-4 sidebar d-none d-lg-block">';

    if ( ! empty( $sidebar_options['map'] ) ){
        echo '<a class="contact--block_map u-animate__fade-up animate" href="'. $contact_link['url'].'" ' . $contact_link_target . '><span class="sr-only">'. $contact_link['title'].'</span>';
        echo '<figure class="u-media-circle u-dropshadow--1">';
        echo $contact_map;
        echo '</figure>';
        echo '</a>';
    }
    
    if ( ! empty( $sidebar_options['show_contact_info']) ){
        echo '<aside class="contact--block_aside" role="complementary">';
        echo \TG\Business_Info::get_address_block( $contact_info );
        echo '</aside>';
    }
    if ( ! empty( $sidebar_options['show_contact_info']) ){
        echo '<aside class="contact--block_aside" role="complementary">';
        echo \TG\Business_Info::get_phone_number( $contact_info ); 
        echo '</aside>';
    }
    
    
    if ( ! empty( $sidebar_content ) ){
        foreach ($sidebar_content as $content_block) {
            echo '<aside class="contact--block_aside" role="complementary">';
            echo '<h3>' . $content_block['section_heading'] . '</h3>';
            echo $content_block['section_content'];
            echo '</aside>';
        }
    }
}  
add_action( \TG\Filters::MAIN_LOOP_AFTER , 'cityservice_sidebar_contact_page_template' , 0);


/**
 * Hook in Blog and Case Study cards into their respective archive pages
 */
function cityservice_blog_card() 
{
    global $post;

    if ( empty( $post )){
        return;
    }
    
    if (  is_home() || is_category() || is_post_type_archive('post') || is_tag() ){
        echo '<div class="col-12 col-xs-12 col-sm-6 col-lg-4">';
        \Component\Factory::build( 'card_blog' , $post );
        echo '</div>';
    
    }elseif( is_post_type_archive('case-study') ){
        echo '<div class="case-study-industry ' . \CITYSERVICE\Functions::taxomony_term_string( $post , 'industry') . '">';
        \Component\Factory::build( 'card_case_study' , $post );
        echo '</div>';
    
    }else{
        return;
    }
}
add_action( \TG\Filters::MAIN_LOOP, 'cityservice_blog_card' , 10 );


/**
 * Hook in Blog and Case Study cards into their respective archive pages
 */
function cityservice_blog_navigation() 
{
    // Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
    }?>
    
    <nav class="container-xl navigation posts-navigation" role="navigation">
        <div class="nav-links">
    		<?php if ( get_next_posts_link() ) : ?>
	    		<div class="nav-previous"><?php next_posts_link( esc_html__( 'Previous Articles', TG()->textdomain ) ); ?></div>
			<?php endif; ?>
        	<?php if ( get_previous_posts_link() ) : ?>
		    	<div class="nav-next"><?php previous_posts_link( esc_html__( 'Newer Articles', TG()->textdomain ) ); ?></div>
			<?php endif; ?>
        </div>
    </nav>

<?php
}
add_action( \TG\Filters::MAIN_LOOP_AFTER, 'cityservice_blog_navigation' , 10 );


/**
 * Filters the length of the excerpt
 */
function cityservice_excerpt_length( $length ) 
{
    
    
    if( get_post_type() == 'post'){
        $length = 35 ;
    
    }elseif( get_post_type() == 'case-study' ){
        $length = 18 ;
    }

    return apply_filters( __FUNCTION__ , $length );
}
add_action( 'excerpt_length', 'cityservice_excerpt_length' , 10 );


/**
 * Filters the "more" string of the excerpt
 */
function cityservice_excerpt_more( $more ) 
{
    
    if( get_post_type() != 'post'){
        return;
    }
    return apply_filters( __FUNCTION__ , ' ...' );

}
add_filter( 'excerpt_more', 'cityservice_excerpt_more' );


/**
 * Custom pre_get_posts action
 */
function cityservice_pre_get_posts( $query ) 
{
    if ( ! is_admin() && $query->is_main_query() ) {
        if ( is_post_type_archive('case-study') ) {
            $query->set( 'posts_per_page', -1 );
        }
    }
}
add_action( 'pre_get_posts', 'cityservice_pre_get_posts' );