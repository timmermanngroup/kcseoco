<?php

namespace Module;

defined('ABSPATH') ?: exit;

class Hero extends Module
{

    const DATA_HANDLE               =   'hero';
    const HERO_TYPE                 =   'hero_type';
    const HEADING                   =   'heading';
    const BACKGROUND_IMAGE          =   'background_image';
    const SUBHEADING                =   'subheading';
    const BUTTON                    =   'button';
    
    
    /**
     * Return the content
     * @access 		public
     * @return 		string
     */
    public function get_hero_data($module){
        
        ob_start();
        
        echo $this->get(self::HEADING) ? '<h1 class="h1 hero__heading">' . \TG\Functions::superscript_filter( $this->get(self::HEADING) ) . '</h1>' : null;

        if ($this->get(self::HERO_TYPE) == 'home'){
            echo $this->get(self::SUBHEADING) ? '<span class="hero__subheading">' . $this->get(self::SUBHEADING) . '</span>' : null;
            echo \TG\Functions::get_button( $this->get(self::BUTTON) , array('class' => 'btn button--circle u-dropshadow--1'));
        }
        
        return apply_filters(__METHOD__, ob_get_clean() );
    }   


    /**
     * Return the content
     * @access 		public
     * @return 		string
     */
    public function get_content($module)
    {
        
        $background_image   =   wp_get_attachment_image_url($this->get(self::BACKGROUND_IMAGE), 'fullsize', false);
        ob_start(); ?>
        
        <div class="hero__wrapper" style="background-image: url(<?php echo $background_image; ?>)">
            <div class="container-xl">
                <div class="row">
                    <div class="col col-lg-12">
                        <?php echo self::get_hero_data($module); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return apply_filters(__METHOD__, ob_get_clean());
    }

}
