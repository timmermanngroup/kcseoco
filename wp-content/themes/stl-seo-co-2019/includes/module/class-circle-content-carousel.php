<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Circle_Content_Carousel extends Module{
  
    const DATA_HANDLE       =   'image_banner';
    const MODULE_HEADING    =   'module_heading';
    const MODULE_SUBHEADING =   'module_sub_heading';
    const MODULE_INTRO      =   'module_introduction_text';
    const MODULE_CONTENT    =   'module_content';
    const MODULE_IMAGE      =   'module_image';
    const IMAGE_CIRCLES     =   'image_circles';
    

    const CONTAINER_SETTING     =   'container_width_setting';
    const COLOR_SETTING         =   'color_setting';
    const IMAGE_SETTING         =   'image_settings';

    
    /**
     * Updates contructor to include class for styling
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function __construct( $module ){
        parent::__construct( $module );
        $this->classes[] = 'image--style__' . $this->get( self::IMAGE_SETTING );
    }


    /**
     * Return the decorative image banner
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_image_banner( $module_content ) 
    {
        
        if ( $this->get( self::IMAGE_SETTING ) != 'fullwidth' ){
            return apply_filters( __METHOD__, __return_false() );
        }

        $module_banner_image    = wp_get_attachment_image( $this->get( self::MODULE_IMAGE ), 'full' , false  );
        
        ob_start();?>
        
        <div class="module--banner">
            <figure class="banner--image">
                <?php echo $module_banner_image ; ?>
            </figure>
        </div>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
    
    /**
     * Return the decorative image circles
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_image_circles( $module_content ) 
    {
        
        if ( $this->get( self::IMAGE_SETTING ) != 'circles' ){
            return apply_filters( __METHOD__, __return_false() );
        }
        
        foreach( $this->get( self:: IMAGE_CIRCLES ) as $key => $value ){
            echo '<figure class="module--media module--media_'. $key .' u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up animate">';
            echo \wp_get_attachment_image( $value['image'] , 'med-card', true , array('class'=>'img-circle') );
            echo '</figure>';
        }

    }

    /**
     * Return the Module content formated into circles
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content_items( $module_content ) 
    {
        
        if ( empty( $module_content ) ){
            return apply_filters( __METHOD__, __return_false() );
        }
        
        ob_start();
        
        echo '<div class="circle-module-carousel owl-carousel">';

        foreach ($module_content as $content_item ) {
            $circle_image = wp_get_attachment_image( $content_item['circle_image'] , 'card', false, array('class'=>'img-circle') );
            $circle_text  = $content_item['circle_text'] ?: null;

            if( $circle_image ) {
                echo '<div class="module--content__circle"><span class="image">' . $circle_image . '</span></div>';
            
            }else if( $circle_text ){
                echo '<div class="module--content__circle"><span>' . $circle_text . '</span></div>';
            } 
        }
        echo '</div>';
        
        return apply_filters( __METHOD__, ob_get_clean() );
    }    
    
    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        $module_heading         = $this->get( self::MODULE_HEADING )    ? '<h2 class="module--heading">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $module_subheading      = $this->get( self::MODULE_SUBHEADING ) ? '<span class="u-animate__border-top">' . $this->get( self::MODULE_SUBHEADING ) . '</span>' : null;
        $module_intro           = $this->get( self::MODULE_INTRO ) ? '<p class="module--introduction">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        $module_content         = $this->get_content_items( $this->get( self::MODULE_CONTENT  ) ) ?: null;
        
        ob_start(); ?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> image--style__<?php echo $this->get( self::IMAGE_SETTING );?>">
            <div class="row">
                <div class="col col-lg-12">
                    <div class="module--header">
                        <?php echo $module_subheading; ?>
                        <?php echo $module_heading; ?>
                        <?php echo $module_intro; ?>
                    </div>
                    <div class="module--content">
                        <?php echo $module_content; ?>
                    </div>
                </div>
            </div>
            
            <?php echo $this->get_image_circles( $this->get( self::MODULE_CONTENT ) ); ?>
        </div>
        
        <?php echo $this->get_image_banner( $this->get( self::MODULE_CONTENT ) ); ?>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}