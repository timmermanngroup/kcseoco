<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Case_Study_Testimonial extends Module{
  
    const DATA_HANDLE           =   'case_study_testimonial';
    const MODULE_CONTENT        =   'module-content';
    const MODULE_IMAGE          =   'module-image';
    
    const TESTIMONIAL           =   'testimonial';
    const TESTIMONIAL_BUSINESS  =   'testimonial_business';
    const TESTIMONIAL_AUTHOR    =   'testimonial_author';
    const TESTIMONIAL_TITLE     =   'testimonial_title';
    

    const CONTAINER_SETTING     =   'container_width_setting';
    const COLOR_SETTING         =   'color_setting';


    /**
     * Return the Module Banner Image
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_banner( $data ) 
    {
        ob_start(); ?>
        <div class="row">
            <figure class="module--banner">
                <?php echo  \wp_get_attachment_image( $this->get( self::MODULE_IMAGE  ) , 'banner', false ) ;?>
            </figure>
        </div>
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );

    }

    /**
     * Return the Single Testimonial Module Layout
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_single_layout( $data ) 
    {

        $testimonial_id      = $this->get( self::MODULE_CONTENT )[0];
        $testimonial_header  = get_post_meta( $testimonial_id, self::TESTIMONIAL_AUTHOR, true) ? '<span class="author">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_AUTHOR, true) . '</span>': null;
        $testimonial_header .= get_post_meta( $testimonial_id, self::TESTIMONIAL_TITLE, true) ? '<span class="title">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_TITLE, true) . '</span>': null;
        $testimonial_content = get_post_meta( $testimonial_id, self::TESTIMONIAL, true) ? '<span class="content">' . get_post_meta( $testimonial_id, self::TESTIMONIAL, true) . '</span>': null;
        
        ob_start(); ?>
        
        <div class="row justify-content-center">
            <section class="col-10 col-xs-10 col-md-8 col-lg-6">
                <div class="module--testimonial">
                    <?php echo $testimonial_header; ?>
                    <?php echo $testimonial_content; ?>
                <div>
            </section>
        </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
    
    
    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        
        ob_start(); ?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?>">
            <?php echo '<a name="testimonial-'. get_the_ID() .'"></a>';?>
            <?php echo $this->get_module_banner( $data ); ?>
            <div class="row module--content module__<?php echo $this->get( self::COLOR_SETTING );?>" >
                <div class="col-12 col-lg-12 offset-xs-1"> 
                    <?php echo self::get_single_layout( $data );?>
                </div>
            </div>
        </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}