<?php

namespace Module;

defined('ABSPATH') ?: exit;

class Hero_Blog_Single extends Module
{

    const DATA_HANDLE               =   'hero';
    const HEADING                   =   'heading';
    const BACKGROUND_IMAGE          =   'background_image';
    
    
    /**
     * Return the content
     * @access 		public
     * @return 		string
     */
    public function get_hero_data($module){
        
        ob_start();
        echo $this->get(self::HEADING) ? '<h1 class="h1 hero__heading">' . $this->get(self::HEADING) . '</h1>' : null;
        
        return apply_filters(__METHOD__, ob_get_clean() );
    }   


    /**
     * Return the content
     * @access 		public
     * @return 		string
     */
    public function get_content($module)
    {
        
        $background_image   =   wp_get_attachment_image_url($this->get(self::BACKGROUND_IMAGE), 'fullsize', false);
        ob_start(); ?>
        
        <div class="hero__wrapper" style="background-image: url(<?php echo $background_image; ?>)">
            <div class="container py-2 py-md-4 py-lg-5">
                <div class="row">
                    <div class="col col-lg-12">
                        <?php \Component\Post_Meta::the_post_meta('combined_meta');?>
                        <?php echo self::get_hero_data($module); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return apply_filters(__METHOD__, ob_get_clean());
    }

}
