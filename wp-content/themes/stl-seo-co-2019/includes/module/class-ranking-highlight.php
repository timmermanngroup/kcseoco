<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Ranking_Highlight extends Module{
  
    const DATA_HANDLE           =   'ranking_hightlight';
    const MODULE_HEADING        =   'module_heading';
    const MODULE_SUBHEADING     =   'module_subheading';
    const MODULE_CONTENT        =   'module_content';
    const MODULE_CONTENT_INTRO  =   'module_content_introduction';
    const CTA_HEADING           =   'cta_heading';
    const CTA_SUBHEADING        =   'cta_sub_heading';
    const CTA_BUTTONS           =   'cta_buttons';
    const CTA_IMAGES            =   'cta_images';
    const CONTAINER_SETTING     =   'container_width_setting';

    
    /**
     * Return the post data
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  array $content_posts
     */
    public function get_module_posts( $data ) 
    {
        return apply_filters( __METHOD__, $this->get( self::MODULE_CONTENT ) );    
    }
    
    
    /**
     * Return the module header
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_header( $data ) 
    {
        $heading        = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $subheading     = $this->get( self::MODULE_SUBHEADING ) ? '<p class="module--subheading">' . $this->get( self::MODULE_SUBHEADING ) . '</p>' : null;
        $introduction   = $this->get( self::MODULE_CONTENT_INTRO ) ? '<span class="module--tagline">' . $this->get( self::MODULE_CONTENT_INTRO ) . '</span>' : null;
        
        ob_start(); ?>
        
        <header class="row module--header">
            <div class="col col-xs-12 col-md-10 col-lg-5 offset-md-2 offset-lg-3">
                <span class="module--heading u-animate__border-top">What we do.</span>
                <?php echo $heading; ?>
                <?php echo $subheading; ?>
                <?php echo $introduction; ?>
            </div>
        </header>
    <?php
    return apply_filters( __METHOD__, ob_get_clean() );
    }   
    
    
    /**
     * Return the module cards
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_cards( $data ) 
    {
        $args = array(
            'post_type'         => 'portfolio',
            'post_status'       => 'publish',
            'posts_per_page'    => -1, 
            'post__in'          => self::get_module_posts( $data ),
            'orderby'           => 'menu_order',
            'order'             => 'ASC',
        );

        $the_query = new \WP_Query( $args );

        if ( $the_query->have_posts() ) {

            echo '<div class="row module--content">';
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                
                        echo '<div class="col-12 col-xs-12 col-md-6 col-xl-4 ranking--card-wrapper">';
                            $card = new \Component\Card_Ranking( get_the_ID() , $args );
                            echo $card->output();
                        echo '</div>';
                }
            echo '</div>';
        }
        wp_reset_postdata();
    }
    

    /**
     * Return the module header
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_footer( $data ) 
    {
        $heading    = $this->get( self::CTA_HEADING ) ? '<p class="module--heading_cta">' . $this->get( self::CTA_HEADING ) . '</p>' : null;
        $subheading = $this->get( self::CTA_SUBHEADING ) ? '<p class="module--subheading_cta">' . $this->get( self::CTA_SUBHEADING ) . '</p>' : null;
        $buttons    = $this->get( self::CTA_BUTTONS ) ? : null;
        $images     = $this->get( self::CTA_IMAGES ) ? : null;
            
        ob_start(); ?>
        
        <footer class="row module--footer">
            <div class="col col-lg-12">
                <div class="module--cta">
                    <?php 
                        echo $heading; 
                        echo $subheading; 
                        foreach ($buttons as $button_data) {
                            echo \TG\Functions::get_button( $button_data['button'] , array( 'class' => 'btn button--pill') );
                        }
                        foreach ($images as $image_data) {
                            echo '<figure class="u-media-circle u-dropshadow--1 u-animate__fade-up">';
                            echo wp_get_attachment_image( $image_data['image'], 'card', false, array() );
                            echo '</figure>';
                        }
                    ?>
                </div>
            </div>
        </footer>
    <?php
    return apply_filters( __METHOD__, ob_get_clean() );
    }  
    

    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        ob_start(); 
        
        echo '<div class="' . $this->get( self::CONTAINER_SETTING ) . '">';
            echo self::get_module_header( $data );
            echo self::get_module_cards( $data );
            echo self::get_module_footer( $data );
        echo '</div>';

        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


