<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Image_Banner extends Module{
  
    const DATA_HANDLE       =   'image_banner';
    const MODULE_HEADING    =   'module_heading';
    const MODULE_INTRO      =   'module_introduction_text';
    const MODULE_CONTENT    =   'module_content';
    const MODULE_IMAGE      =   'module_image';

    const CONTAINER_SETTING     =   'container_width_setting';
    const COLOR_SETTING         =   'color_setting';
    const CONTENT_SETTING       =   'content_setting';

    
    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {

        $page_intro = $this->get( self::MODULE_INTRO ) ? '<p class="introduction--snippet">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        $heading    = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading u-animate__border-top">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $content    = $this->get( self::MODULE_CONTENT ) ? $this->get( self::MODULE_CONTENT ) : null;
        $image      = wp_get_attachment_image( $this->get( self::MODULE_IMAGE ), 'full' , false , array('class' => 'img-circle' ) ) ?: null;
        
        $media_size =  empty( $content ) ?  'module--media__lg' : 'module--media__sm';

        ob_start(); ?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?>">
            <div class="row">
                <div class="col col-lg-12">
                    
                    <div class="module--banner">
                        <span class="banner--magnifier u-animate__fade-up d-none d-lg-block"></span> 
                        <span class="banner--magnifier u-animate__fade-up d-none d-lg-block"></span> 
                        <figure class="banner--image">
                            <?php echo $image; ?>
                        </figure>
                    </div>

                    <div class="module--content module__<?php echo $this->get( self::COLOR_SETTING );?>">
                        <div class="module--header">
                            <?php echo $heading; ?>
                        </div>
                        <div class="module--introduction">
                            <?php echo $page_intro; ?>
                        </div>
                        <div class="u-columned__3">
                            <?php echo $content; ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}