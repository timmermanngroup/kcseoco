<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Page_Introduction extends Module{
  
    const DATA_HANDLE       =   'page_introduction';
    const MODULE_HEADING    =   'module_heading';
    const MODULE_INTRO      =   'module_introduction_text';
    const MODULE_CONTENT    =   'module_content';
    const MODULE_BUTTON     =   'module_button';
    const MODULE_IMAGE      =   'module_image';
    const MODULE_IMAGES     =   'module_images';

    const CONTAINER_SETTING     =   'container_width_setting';
    const STYLE_SETTING         =   'style_setting';
    const BUTTON_SETTING        =   'button_setting';

    /**
     * Return the Module Button
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_button( $data ) 
    {
        $button = \TG\Functions::get_button( $data[self::MODULE_BUTTON] , array('class' => 'btn btn__icon ' . $data[self::BUTTON_SETTING] ));
        
        return apply_filters( __METHOD__, $button );    
    }
    
    /**
     * Return the Module markup for the homepage
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_homepage_markup( $data ) 
    {
        $page_intro = $this->get( self::MODULE_INTRO ) ? '<p class="introduction--snippet">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        $heading    = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading u-animate__border-top">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $content    = $this->get( self::MODULE_CONTENT ) ? $this->get( self::MODULE_CONTENT ) : null;
        $images     = $this->get( self::MODULE_IMAGES ) ?: null;
        $button     = $this->get_button( $data );

        ob_start();?>

        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> front-page">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="module--images">

                        <?php 
                            if ( $images ){
                                foreach ($images  as $image) {
                                    echo '<figure class="module--media module--media__sm u-media-circle d-none d-lg-block u-animate__fade-up animate">';
                                    echo wp_get_attachment_image( $image['image'] , 'full' , false , array('class' => 'img-circle' ) );
                                    echo '</figure>';
                                }
                            }
                            ?>    
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-2">
                    <div class="module--header">
                        <?php echo $heading; ?>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="module--introduction">
                        <?php echo $page_intro; ?>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="module--content">
                        <?php echo $content; ?>
                        <?php echo $button; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
    
    /**
     * Return the Module markup for the imternal pages
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_internal_markup( $data ) 
    {
        $page_intro     = $this->get( self::MODULE_INTRO ) ? '<p class="introduction--snippet">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        $heading        = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading u-animate__border-top">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $content        = $this->get( self::MODULE_CONTENT ) ? $this->get( self::MODULE_CONTENT ) : null;
        $image          = wp_get_attachment_image( $this->get( self::MODULE_IMAGE ), 'med-card' , false , array('class' => 'img-circle' ) ) ?: null;
        $button         = $this->get_button( $data );
        $media_size     =  empty( $content ) ?  'module--media__lg' : 'module--media__sm';

        ob_start();?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?>">
            <div class="row">
                <div class="col col-lg-12">
                    
                    <div class="module--introduction">
                        <?php if(( $data[self::STYLE_SETTING  ] == 'case-study-intro' ) || ( $data[self::STYLE_SETTING  ] == 'blog-post-intro' ) ){
                                echo $button;
                            }?>
                        <?php echo $page_intro; ?>
                    </div>
                    <div class="module--header">
                        <?php echo $heading; ?>
                    </div>
                    <div class="module--content">
                        <?php echo $content; ?>
                        <?php echo $button; ?>
                    </div>
                    <figure class="module--media <?php echo $media_size;?> u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up">
                        <?php echo $image; ?>
                    </figure>
                    
                </div>
            </div>
        </div>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }

    /**
     * Return the Module markup for the Case Studies
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_case_study_markup( $data ) 
    {
        $page_intro     = $this->get( self::MODULE_INTRO ) ? '<p class="introduction--snippet">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        $heading        = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading u-animate__border-top">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $content        = $this->get( self::MODULE_CONTENT ) ? $this->get( self::MODULE_CONTENT ) : null;
        $image          = wp_get_attachment_image( $this->get( self::MODULE_IMAGE ), 'card' , false , array('class' => 'img-circle' ) ) ?: null;
        $button         = $this->get_button( $data );
        $media_size     =  empty( $content ) ?  'module--media__lg' : 'module--media__sm';

        ob_start();?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> case-study">
            <div class="row">
                <div class="col col-lg-12">
                    
                    <div class="module--introduction">
                        <a href="<?php echo get_bloginfo('url'); ?>/our-work/case-studies/" target="" title="Read additional case studies." class="btn btn__icon btn__l_arrow d-none d-md-block">All Case Studies.</a> 
                        <?php echo $page_intro; ?>
                    </div>
                    <div class="module--header">
                        <?php echo $heading; ?>
                    </div>
                    <div class="module--content">
                        <?php echo $content; ?>
                        <?php echo $button; ?>
                    </div>
                    <?php if ( $image ){
                        echo '<figure class="module--media ' . $media_size . ' u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up">';
                        echo $image;
                        echo '</figure>';
                        }?>
                </div>
            </div>
        </div>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }

    /**
     * Return the Module markup for the Case Studies
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_blog_markup( $data ) 
    {
        $page_intro     = $this->get( self::MODULE_INTRO ) ? '<p class="introduction--snippet">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        //$heading        = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading u-animate__border-top">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $content        = $this->get( self::MODULE_CONTENT ) ? $this->get( self::MODULE_CONTENT ) : null;
        $image          = wp_get_attachment_image( $this->get( self::MODULE_IMAGE ), 'card' , false , array('class' => 'img-circle' ) ) ?: null;
        $button         = $this->get_button( $data );
        $media_size     =  empty( $content ) ?  'module--media__lg' : 'module--media__sm';

        ob_start();?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> blog-single">
            <div class="row">
                <div class="col col-lg-12">
                    
                    <div class="module--introduction">
                        <a href="<?php echo get_bloginfo('url'); ?>/blog/" target="" title="Read additional blog posts." class="btn btn__icon btn__l_arrow d-none d-md-block">All Blog Posts</a> 
                        <?php echo $page_intro; ?>
                    </div>
                    <div class="module--content">
                        <?php echo $content; ?>
                    </div>
                    <?php if ( $image ){
                        echo '<figure class="module--media ' . $media_size . ' u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up">';
                        echo $image;
                        echo '</figure>';
                        }?>
                </div>
            </div>
        </div>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }

    /**
     * Return the Module markup for the Case Studies
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_archive_markup( $data ) 
    {
        $page_intro     = $this->get( self::MODULE_INTRO ) ? '<p class="introduction--snippet">' . $this->get( self::MODULE_INTRO ) . '</p>' : null;
        $image          = wp_get_attachment_image( $this->get( self::MODULE_IMAGE ), 'card' , false , array('class' => 'img-circle' ) ) ?: null;
        $button         = $this->get_button( $data );

        ob_start();?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> archive-page">
            <div class="row">
                <div class="col col-lg-12">
                    
                    <div class="module--introduction">
                        <?php echo $button; ?>
                        <?php echo $page_intro; ?>
                    </div>
                    <?php if ( $image ){
                        echo '<figure class="module--media module--media__sm u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up">';
                        echo $image;
                        echo '</figure>';
                        }?>
                </div>
            </div>
        </div>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
    
    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        
        ob_start();

        if( $data[self::STYLE_SETTING  ] == 'home-page-intro' ){
            echo $this->get_homepage_markup( $data  );
        }elseif( $data[self::STYLE_SETTING  ] == 'case-study-intro' ){
            echo $this->get_case_study_markup( $data );
        }elseif( $data[self::STYLE_SETTING  ] == 'blog-post-intro' ){
            echo $this->get_blog_markup( $data );
        }elseif( $data[self::STYLE_SETTING  ] == 'archive-page-intro' ){
            echo $this->get_archive_markup( $data );
        }else{
            echo $this->get_internal_markup( $data );
        }
        
        return apply_filters( __METHOD__, ob_get_clean() );
    }
    
}


