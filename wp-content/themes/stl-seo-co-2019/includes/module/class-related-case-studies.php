<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Related_Case_Studies extends Module{
  
    const DATA_HANDLE           =   'related_case_studies';
    const MODULE_HEADING        =   'module_heading';
    const MODULE_CONTENT        =   'module_content';
    const MODULE_BUTTON         =   'module_button';
    const CONTENT_SETTING       =   'content_setting';
    const CONTAINER_SETTING     =   'container_width_setting';
    const BUTTON_SETTING        =   'button_setting';

    

    /**
     * Return the post data
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  array $content_posts
     */
    public function get_module_posts( $data ) 
    {
        return apply_filters( __METHOD__, $this->get( self::MODULE_CONTENT ) );    
    }
    
    
    /**
     * Return the module header
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_header( $data ) 
    {
        $heading    = $this->get( self::MODULE_HEADING ) ? '<h3 class="module--heading">' . $this->get( self::MODULE_HEADING ) . '</h3>' : null;
        
        ob_start(); ?>
        
        <header class="row module--header">
            <div class="col col-xs-12 col-md-10 col-lg-8">
                <?php echo $heading; ?>
            </div>
        </header>
    <?php
    return apply_filters( __METHOD__, ob_get_clean() );
    }   
    
    
    /**
     * Return the module cards
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_cards( $data ) 
    {
        $args = array(
            'post_type'         => 'case-study',
            'posts_per_page'    => -1, 
            'post__in'          => self::get_module_posts( $data ),
            'order'             => 'DESC',
        );

        $the_query = new \WP_Query( $args );

        if ( $the_query->have_posts() ) {

            echo '<div class="row module--content align-items-stretch">';
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                
                        echo '<div class="col-12 col-xs-12 col-sm-6 col-lg-4 col-xl-3">';
                            $card = new \Component\Card_Case_Study( 'card_case_study',  get_the_ID());
                            echo $card->output();
                        echo '</div>';
                }
            echo '</div>';
        }
        wp_reset_postdata();
    }
    

    /**
     * Return the module footer
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_footer( $data ) 
    {
        $button    = $this->get( self::MODULE_BUTTON  ) ? : null;
        $button_class = $this->get( self::BUTTON_SETTING ) ?  'btn btn__icon ' . $this->get( self::BUTTON_SETTING ) : null;
          
        ob_start(); ?>
        
        <footer class="row module--footer">
            <div class="col col-lg-12">
                <div class="module--cta">
                    <?php echo \TG\Functions::get_button( $button , array( 'class' => $button_class ) ); ?>
                </div>
            </div>
        </footer>
    <?php
    return apply_filters( __METHOD__, ob_get_clean() );
    }  
    

    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        ob_start(); 
        
        echo '<div class="' . $this->get( self::CONTAINER_SETTING ) . '">';
        echo self::get_module_header( $data );
        echo self::get_module_cards( $data );
        echo self::get_module_footer( $data );
        echo '</div>';
        
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


