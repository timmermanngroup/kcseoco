<?php

namespace Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


abstract class Module {

	const LAYOUT 		= 	'acf_fc_layout';
	const ID_PREFIX 	= 	'module--';

	/**
	 * The ID for this instance of the class
	 * @access 	public
	 * @var 	string
	 */
	public $ID;

	/**
	 * The layout name speficied in ACF
	 * @access 	public
	 * @var 	string
	 */
	public $layout 	= 	'';


	/**
	 * The array of class names
	 * @access 	public
	 * @var 	array
	 */
	public $classes 	= 	['module__content'];	


    /**
     * The data for the Class
     * @access  public
     * @var     string
     */
    public $data =  '';


	/**
	 * The main constructor method
	 * @access 	public
	 * @param  	array
	 * @return 	void
	 */
	public function __construct( $module ) {
        $this->layout 		= 	strtolower( $module[self::LAYOUT] );
        $this->classes[] 	= 	self::ID_PREFIX . str_replace( [' ', '_'], '-', $this->layout );

		$this->ID 			= 	apply_filters( __METHOD__ . '/ID', uniqid( self::ID_PREFIX ) );
		$this->classes 		= 	apply_filters( __METHOD__ . '/classes', $this->classes, $this->ID, $this->layout );
        $this->data         =   apply_filters( __METHOD__, $module, $this->ID, $this->layout );
	}

    
    /**
	 * Return the content
	 * @access 		protected
	 * @abstract
	 * @return 		void
	 */
    protected abstract function get_content( $module );
    
    

    /**
	 * Return the module anchor
	 * @access 		protected
	 * @abstract
	 * @return 		void
	 */
    protected function get_anchor( $module )
    {
        
        if ( empty( $this->get( 'module_anchor' ) ) ){
            return;
        } 
        $module_anchor = '<div id="' . \CITYSERVICE\Functions::get_concatenated_string( $this->get( 'module_anchor' )) . '"></div>';
        
        return apply_filters(__METHOD__, $module_anchor );
    }
    
    
    /**
	 * Gets the module data
	 * @access 		protected
	 * @abstract
	 * @return 		void
	 */   
    public function get($key)
    {
        if (isset($this->data[$key]) && !empty($this->data[$key])) {
            return apply_filters(__METHOD__, $this->data[$key], $key);
        }
    }


	/**
	 * Return the Module HTML
	 * @access 	public
	 * @return 	string
	 */
	public function get_html() {

        $html 	= 	sprintf(
			'<div id="%2$s" class="%3$s">%4$s %1$s</div>',
            $this->get_content( $this->data ),
			$this->ID,
            join( ' ', $this->classes ),
            $this->get_anchor( $this->data )
        );

		return $html;
	}



	/**
	 * Build the Module HTML
	 * @access 	public
	 * @return 	string
	 */
	public function build_html() {
        
        print $this->get_html();
    }
}