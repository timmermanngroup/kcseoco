<?php

namespace Module;

defined('ABSPATH') ?: exit;

class Cta extends Module
{

    const DATA_HANDLE               =   'cta';
    const HEADING                   =   'module_heading';
    const SUB_HEADING               =   'module_sub_heading';
    const BUTTON                    =   'module_button';
    const IMAGE                     =   'module_image';
    const FORM                      =   'cta_form';

    const CONTAINER_SETTING         =   'container_width_setting';
    const COLOR_SETTING             =   'color_setting';
    const CONTENT_SETTING           =   'content_setting';
    const SECONDARY_CONTENT_SETTING =   'secondary_content_setting';
    const BUTTON_SETTING            =   'button_setting';
    

    /**
     * Return the Module Button
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_button($data)
    {
        if ($this->get(self::SECONDARY_CONTENT_SETTING) == 'content-form') {
            return;
        }

        $button = \TG\Functions::get_button($data[self::BUTTON], array('class' => 'btn btn--color-white ' . $this->get(self::BUTTON_SETTING) . ' u-dropshadow--1'));

        return apply_filters(__METHOD__, $button);
    }

    /**
     * Return the Content Setting
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content_setting($data)
    {
        $content_setting = $this->get(self::CONTENT_SETTING);
        
        if ($this->get(self::BUTTON_SETTING) == 'button--circle') {
            $content_setting .= ' content-' . $this->get(self::BUTTON_SETTING);
        }

        if ($this->get(self::SECONDARY_CONTENT_SETTING) == 'content-form') {
            $content_setting .= ' ' . $this->get(self::SECONDARY_CONTENT_SETTING);
        }

        return apply_filters(__METHOD__, $content_setting);
    }


    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content($data)
    {

        $content_setting  = $this->get_content_setting($data);
        $content          = $this->get(self::HEADING) ? '<h3 class="h1 cta__heading">' . $this->get(self::HEADING) . '</h3>' : null;
        $content         .= $this->get(self::SUB_HEADING) ? '<p class="h3 cta__subheading">' .  $this->get(self::SUB_HEADING) . '</p>' : null;

        $button_setting   = $this->get(self::BUTTON_SETTING);
        $button           = $this->get_button($data) ? $this->get_button($data) : null;
        $image            = wp_get_attachment_image($this->get(self::IMAGE), 'large-card', false, array('class' => 'img-circle'));
        $form             = $this->get(self::FORM) ? $this->get(self::FORM) : null;

        ob_start(); ?>

        <div class="<?php echo $this->get(self::CONTAINER_SETTING); ?> module__<?php echo $this->get(self::COLOR_SETTING); ?>">
            <div class="container">
                <div class="row justify-content-between <?php echo $content_setting; ?>">

                    <div class="col-12 col-lg-6 cta__content">
                        <?php echo $content; ?>
                        <?php if ($button_setting == 'button--pill') {
                            echo $button;
                            } ?>
                    </div>
                    
                    <?php 
                    if ($this->get(self::SECONDARY_CONTENT_SETTING) == 'content-form'){
                        echo '<div class="col-12 col-lg-4 offset-lg-1">';
                        echo do_shortcode('[gravityform id='. $form .' title=false description=false ajax=true tabindex=49]');
                        echo '</div>';
                    }else{
                        echo '<div class="col-12 col-md-6">';
                        echo '<figure class="cta__media u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up">' . $image . '</figure>';
                        echo '</div>';
                    }?>
                    
                    <?php if ($button_setting == 'button--circle') { 
                        echo $button; 
                        } ?>
                </div>
            </div>
        </div>

<?php

        return apply_filters(__METHOD__, ob_get_clean());
    }
}
