<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Basic_Content extends Module{
  
    const DATA_HANDLE   =   'basic_content';
    const CONTENT       =   'module_content';
    const BUTTON        =   'module_button';
    const IMAGE         =   'module_image';

    /**
     * Return the Module Button
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_button( $data ) 
    {
        $button = \TG\Functions::get_button( $data[self::BUTTON] , array('class' => 'button' ));
        return apply_filters( __METHOD__, $button );    
    }
    
    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {

        $content  = $this->get( self::CONTENT ) ? $this->get( self::CONTENT ) : null;
        $image    = wp_get_attachment_image( $this->get( self::IMAGE ), 'fullsize' , false , array('class' => 'img-circle' ) ) ?: null;
        $button   = $this->get_button( $data );
        
        ob_start(); ?>

            <div class="container py-2 py-md-4 py-lg-5">
                <div class="row">
                    <div class="col col-lg-12">
                        <?php echo $content; ?>
                        <?php echo $button; ?>
                        <?php echo $image; ?>
                    </div>
                </div>
            </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


