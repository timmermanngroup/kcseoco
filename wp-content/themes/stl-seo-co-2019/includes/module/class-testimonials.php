<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Testimonials extends Module{
  
    const DATA_HANDLE           =   'testimonials';
    const MODULE_HEADING        =   'heading';
    const SINGLE_TEST_MODULE    =   'single_testimonial';
    const MULTI_TEST_MODULE     =   'testimonial_carousel';
    

    const TESTIMONIAL           =   'testimonial';
    const TESTIMONIAL_BUSINESS  =   'testimonial_business';
    const TESTIMONIAL_AUTHOR    =   'testimonial_author';
    const TESTIMONIAL_TITLE     =   'testimonial_title';
    const TESTIMONIAL_IMAGE     =   'testimonial_image';
    

    const CONTAINER_SETTING     =   'container_width_setting';
    const COLOR_SETTING         =   'color_setting';
    const CONTENT_SETTING       =   'content_setting';


    
    /**
     * Return the Single Testimonial Module Layout
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_single_layout( $data ) 
    {

        $testimonial_id = $this->get( self::SINGLE_TEST_MODULE )[0];
        $testimonial_header  = get_post_meta( $testimonial_id, self::TESTIMONIAL_BUSINESS, true) ? '<span class="heading">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_BUSINESS, true) . '</span>': null;
        $testimonial_header .= get_post_meta( $testimonial_id, self::TESTIMONIAL_AUTHOR, true) ? '<span class="author">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_AUTHOR, true) . '</span>': null;
        $testimonial_header .= get_post_meta( $testimonial_id, self::TESTIMONIAL_TITLE, true) ? '<span class="title">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_TITLE, true) . '</span>': null;
        $testimonial_content = get_post_meta( $testimonial_id, self::TESTIMONIAL, true) ? '<span class="content">' . get_post_meta( $testimonial_id, self::TESTIMONIAL, true) . '</span>': null;
        
        ob_start(); ?>
        <div class="row">
            <header class="module--header col-12 col-xs-12 col-lg-4">
                <?php echo $testimonial_header; ?>
            </header>
            <section class="module--content col-12 col-xs-12 col-lg-8">
                <?php echo $testimonial_content; ?>
            </section>
        </div>
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }

    
    
    
    
    
    
    /**
     * Return the Testimonial Carousel Module Layout
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_carousel_header( $data ) 
    {
        ob_start();?>

        <header class="module--header">
            <h2 class="module--heading u-animate__border-top">Testimonials.</h2>
            <?php echo $this->get( self::MULTI_TEST_MODULE )['heading'] ? '<h3 class="module--subheading">' . $this->get( self::MULTI_TEST_MODULE )['heading'] . '</h3>' : null ; ?>
        </header>
        
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }    
    
    
    /**
     * Return the Testimonial Carousel Module Layout
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_carousel_layout( $data ) 
    {

        $testimonial_data = $this->get( self::MULTI_TEST_MODULE );

        ob_start(); ?>

        <div class="module--carousel carousel_testimonial owl-carousel"><!--  --->

        <?php foreach ( $testimonial_data[ self::DATA_HANDLE ] as $testimonial_id ) :
            
            
            $testimonial_content = get_post_meta( $testimonial_id, self::TESTIMONIAL, true) ? '<span class="content">' . get_post_meta( $testimonial_id, self::TESTIMONIAL, true) . '</span>': null;
            $testimonial_header = get_post_meta( $testimonial_id, self::TESTIMONIAL_AUTHOR, true) ? '<span class="author">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_AUTHOR, true) . '</span>': null;
            $testimonial_header .= get_post_meta( $testimonial_id, self::TESTIMONIAL_TITLE, true) ? '<span class="title">' . get_post_meta( $testimonial_id, self::TESTIMONIAL_TITLE, true) . '</span>': null;
            
            $testimonial_image  =  \wp_get_attachment_image( get_post_meta( $testimonial_id, self::TESTIMONIAL_IMAGE, true) , 'full', false );
            
            ?>
        
            <div class="carousel--item col col-xs-12"> <!--  --->
                <header class="testimonial--header">
                    <?php echo $testimonial_header; ?>
                </header>
                <section class="testimonial--content">
                    <?php echo $testimonial_content; ?>
                </section>
                
                <?php if( $testimonial_image ){ ?>
                    <figure class="testimonial--image u-media-circle u-dropshadow--1">
                        <?php echo $testimonial_image; ?>
                    </figure>
                <?php } ?>
                
            
            </div>

        <?php endforeach; ?>

        </div>
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
    
    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        
        ob_start(); ?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> module__<?php echo $this->get( self::COLOR_SETTING );?>">
            <div class="row">
                <div class="col col-lg-12">        
                    <?php 
                        switch ( $this->get( self::CONTENT_SETTING ) ) {
                            case 'testimonial-single':
                                echo self::get_single_layout( $data );
                                break;
                            
                            case 'testimonial-carousel':
                                echo self::get_carousel_header( $data );
                                echo self::get_carousel_layout( $data );
                                break;
                            
                            default: 
                                echo self::get_single_layout( $data );
                        }?>
                </div>
            </div>
        </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


