<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Blog extends Module{
  
    const DATA_HANDLE           =   'page_introduction';
    const MODULE_HEADING        =   'module_heading';
    const MODULE_CONTENT        =   'module_content';
    const MODULE_BUTTON         =   'module_button';

    const CONTAINER_SETTING     =   'container_width_setting';
    const CONTENT_SETTING       =   'content_setting';

    
    /**
     * Return the post data
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  array $content_posts
     */
    public function get_module_posts( $data ) 
    {
        if( $this->get( self::CONTENT_SETTING ) != 'select-posts' ){
            return null;
        }

        $selected_posts = $this->get( self::MODULE_CONTENT );
        
        if ( $selected_posts ){
            //remove current post from selected post array
            if ( ( $key = array_search( get_the_ID() ,  $selected_posts  )  ) !== false ) {
                unset( $selected_posts[$key] );
            }
        }
        
        return apply_filters( __METHOD__, $selected_posts );    
    }
    
    
    /**
     * Return the module cards
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_cards( $data ) 
    {
        $args = array(
            'post_type'         => 'post',
            'posts_per_page'    => 3, 
            'order'             => 'DESC',
        );

        if( self::get_module_posts( $data ) ){
            $args['post__in'] = self::get_module_posts( $data );
        }else{
            $args['post__not_in'] = array( get_the_ID() );
        }
        
        $the_query = new \WP_Query( $args );

        // The Loop
        if ( $the_query->have_posts() ) {
            while ( $the_query->have_posts() ) {
                $the_query->the_post();

                echo '<div class="col-12 col-xs-12 col-md-6 col-lg-4">';
                \Component\Factory::build( 'card_blog' , $data );
                echo '</div>';
            }
        }
        wp_reset_postdata();
    }
    
    
    /**
     * Return the Module Button
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_button( $data ) 
    {
        $button = \TG\Functions::get_button( $data[self::MODULE_BUTTON] , array('class' => 'btn btn__icon btn__r_arrow' ));
        return apply_filters( __METHOD__, $button );    
    }
    

    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {

        $heading = $this->get( self::MODULE_HEADING ) ? '<h3 class="module--subheading">' . $this->get( self::MODULE_HEADING ) . '</h3>' : null;
        $button     = $this->get_button( $data );
        
        ob_start(); ?>
        
        <div class="<?php echo $this->get( self::CONTAINER_SETTING );?> pb-2 pb-md-4 pb-lg-5">
            <div class="row">
                <div class="col col-lg-12">
                    <div class="module--header">
                        <?php echo '<h2 class="module--heading u-animate__border-top">Our Blog.</h2>' ?>
                    </div>
                    <div class="module--introduction">
                        <?php echo $heading; ?>
                    </div>
                </div>
            </div>
            <div class="row module--content">
                <?php echo self::get_module_cards( $data );?>
            </div>
            <div class="row module--footer">
                <div class="col col-lg-12">
                    <?php echo $button; ?>
                </div>
            </div>
        </div>
        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


