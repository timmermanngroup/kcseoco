<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;


class Case_Study_Portfolio extends Module
{
  
    const DATA_HANDLE           =   'case_study_portfolio';
    const MODULE_HEADING        =   'module_heading';
    const MODULE_SUBHEADING     =   'module_subheading';
    const MODULE_CONTENT        =   'module_content';
    const MODULE_WEBSITE        =   'module_website';
    const CONTAINER_SETTING     =   'container_width_setting';

    
    /**
     * Return the post data
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  array $content_posts
     */
    public function get_module_posts( $data ) 
    {
        return apply_filters( __METHOD__, $this->get( self::MODULE_CONTENT ) );    
    }
    
    
    /**
     * Return the module header
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_header( $data ) 
    {
        $heading    = $this->get( self::MODULE_HEADING ) ? '<h2 class="module--heading">' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $subheading = $this->get( self::MODULE_SUBHEADING ) ? '<p class="module--subheading">' . $this->get( self::MODULE_SUBHEADING ) . '</p>' : null;
        
        ob_start(); ?>
        
        <header class="row module--header">
            <div class="col-10 col-md-10 col-lg-9">
                <?php echo \TG\Functions::superscript_filter( $heading ); ?>
                <?php echo $subheading; ?>
            </div>
        </header>
    <?php
    return apply_filters( __METHOD__, ob_get_clean() );
    }   
    
    
    /**
     * Return the module cards
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_cards( $data ) 
    {
        $args = array(
            'post_type'         => 'portfolio',
            'posts_per_page'    => -1, 
            'post__in'          => self::get_module_posts( $data ),
            'order'             => 'DESC',
        );

        $the_query = new \WP_Query( $args );

        if ( $the_query->have_posts() ) {

            echo '<div class="row module--content align-items-stretch">';
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                
                        echo '<div class="col-12 col-xs-12 col-sm-6 col-lg-4">';
                            $args = array(
                                'display_client'        => FALSE,
                                'display_client_url'    => FALSE,
                            );
                            $card = new \Component\Card_Ranking( get_the_ID() , $args );
                            echo $card->output();
                        echo '</div>';
                }
            echo '</div>';
        }
        wp_reset_postdata();
    }
    

    /**
     * Return the module footer
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_module_footer( $data ) 
    {
        $website    = $this->get( self::MODULE_WEBSITE ) ? \TG\Functions::clean_urls( $this->get( self::MODULE_WEBSITE ) ) : null;
            
        ob_start(); ?>
        
        <footer class="row module--footer">
            <div class="col col-lg-12">
                <?php echo $website;?>
            </div>
        </footer>
    <?php
    return apply_filters( __METHOD__ , ob_get_clean() );
    }  
    

    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        //$heading    = $this->get( self::CTA_HEADING ) ? '<p class="module--heading_cta">' . $this->get( self::CTA_HEADING ) . '</p>' : null;

        ob_start(); 
        
        echo '<div class="' . $this->get( self::CONTAINER_SETTING ) . '">';
        echo self::get_module_header( $data );
        echo self::get_module_cards( $data );
        echo self::get_module_footer( $data );
        echo '</div>';

        return apply_filters( __METHOD__ , ob_get_clean() );
    }
}


