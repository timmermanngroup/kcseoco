<?php

namespace Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Factory {



	/**
	 *  Return Module Class
	 *
	 * @param 	array[mixed] 	$module
	 * @return 	stdClass
	 */
	public static function get_module( $module = [] ) {
		if( empty( $module ) ) {
			throw new \Exception( 'Empty Module Content' );			

		} else {
			$layout 		= 	$module[Module::LAYOUT];
			$class_name 	= 	__NAMESPACE__ . '\\' . self::get_class_name( $layout );

			if( class_exists( $class_name ) ) {
				return new $class_name( $module );

			} else {
				throw new \Exception( "<strong>$class_name</strong> not found" );				
			}
		}
	}



	/**
	 * Return HMTL of called Module
	 * @access 	public
	 * @static
	 * @param 	array 		$module
	 * @return 	void
	 */
	public static function get_html( $module = array() ) {
		$module = self::get_module( $module );

		$module->get_html();
	}



	/**
	 * Build HMTL of called Module
	 * @access 	public
	 * @static
	 * @param 	array 		$module
	 * @return 	void
	 */
	public static function build_html( $module = array() ) {
		$module   =  self::get_module( $module );
		
		$module->build_html();
	}



	/**
	 * Make the class name
	 * @access 	private
	 * @static
	 * @param 	string 		$layout
	 * @return 	string
	 */
	private static function get_class_name( $layout = '' ) {
		if( $layout != '' ) {
			$layout 	= 	str_replace( array( '-', '_' ), ' ', strtolower( $layout ) );
			$layout 	= 	explode( ' ' , $layout );

			foreach( $layout as $string ) {
				$keys[] 	=	ucfirst( $string );
			}

			$layout 	= 	join( '_', $keys );
		}

		return $layout;
	}
}