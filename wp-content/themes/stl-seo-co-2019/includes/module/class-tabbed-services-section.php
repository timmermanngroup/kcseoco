<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Tabbed_Services_Section extends Module{
  
    const DATA_HANDLE           = 'tabbed_services_section';
    const MODULE_HEADING        = 'module_heading';
    const MODULE_SUBHEADING     = 'module_sub_heading';
    const MODULE_CONTENT        = 'module_content';
    const MODULE_INTRODUCTION   = 'module_introduction_text';
    const MODULE_IMAGE          = 'module_image';

    const SERVICE_TAB           = 'service_tab_text';
    const SERVICE_HEADING       = 'service_heading';
    const SERVICE_SUBHEADING    = 'service_subheading';
    const SERVICE_CONTENT       = 'service_content';
    const SERVICE_BUTTON        = 'service_button';
    const SERVICE_IMAGE         = 'service_image';
    const SERVICE_ICON          = 'service_icon';


    const MODULE_CONTAINER_SETTING = 'container_width_setting';
    const SERVICE_CONTENT_SETTING  = 'content_column_options';

    /**
     * Return the Service Slug
     * @access  public 
     * @static
     * @param   string   $service
     * @return  string
     */
    public function get_service_slug( $service ) 
    {
        $service = strtolower( join('-', explode( ' ' , $service ) ) );
        
        return apply_filters( __METHOD__, $service ) ;    
    }
    
    /**
     * Return the Module Button
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_button( $data ) 
    {
        $button = \TG\Functions::get_button( $data , array('class' => 'btn button--pill' ));
        return apply_filters( __METHOD__, $button );    
    }
    

    /**
     * Return the Services Tab Navigation
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_service_navigation( $data ) 
    {

        ob_start(); ?>
        
        <nav class="service__navigation">
        
        <?php
            foreach( $this->get( self::MODULE_CONTENT ) as $service ){   
                $service_slug   = $this->get_service_slug( $service[self::SERVICE_TAB] ) ?: null;
                $service_tab    = $service[self::SERVICE_TAB ] ?: null;
                echo '<a href="#section-'. $service_slug .'" class="">'. $service_tab .'</a>';
            }?>
        
        </nav>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );    
    } 

    /**
     * Return the Services Markups
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_service_markup( $data ) 
    {
        ob_start();
        
        echo '<section class="service__content">';

        foreach( $this->get( self::MODULE_CONTENT ) as $service ){
        
            $service_slug       = $this->get_service_slug( $service[self::SERVICE_TAB] ) ?: null;
            $service_heading    = $service[self::SERVICE_HEADING ] ? '<h2 class="service__title">' . $service[self::SERVICE_HEADING ]  . '</h2>' : null;
            $service_subheading = $service[ self::SERVICE_SUBHEADING ] ? '<p>' . $service[ self::SERVICE_SUBHEADING ]  . '</p>' : null;
            $service_content    = $service[self::SERVICE_CONTENT ] ? '<p>' . $service[self::SERVICE_CONTENT ]  . '</p>' : null;
            $service_button     = $service[self::SERVICE_BUTTON ] ? $this->get_button( $service[self::SERVICE_BUTTON ] ) : null;
            $service_image      = wp_get_attachment_image( $service[self::SERVICE_IMAGE ] , 'large-card' , false , array('class'=>'img-circle'));
            $service_icon       = wp_get_attachment_image( $service[self::SERVICE_ICON ] , 'card' , false , array('class'=>'img-circle'));
            ?>

            <article id="section-<?php echo $service_slug;?>" class="row justify-content-between <?php echo $service[self::SERVICE_CONTENT_SETTING];?>">
                <div class="col-12 col-lg-6">
                    <header>
                        <?php echo $service_heading; ?>
                        <?php echo $service_subheading ;?>
                    </header>
                    <section>
                        <?php echo $service_content ;?>
                        <?php echo $service_button ;?>
                    </section>
                </div>
                <footer class="col-12 col-md-6">
                    <div class="service__images">
                       
                        <?php if( $service_image ){ ?>
                            <figure class="module--media u-media-circle u-dropshadow--1 d-none d-lg-block u-animate__fade-up">
                                <?php echo $service_image ;?>
                            </figure>
                        <?php } ?>
                        <?php if( $service_icon ){ ?>
                            <figure class="module--icon d-none d-lg-block u-animate__fade-up">
                                <?php echo $service_icon ;?>
                            </figure>
                        <?php } ?>
                        

                    </div>
                </footer>
            </article>
        
        <?php } 

        echo '</section>';

        return apply_filters( __METHOD__, ob_get_clean() );    
    } 


    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {
        ob_start(); ?>



        
        <div id="section-services" class="<?php echo $this->get( self::MODULE_CONTAINER_SETTING );?> py-2 py-md-4 py-lg-5 ">
            <div class="row">
                <div class="col col-lg-12">
                    <?php print $this->get_service_navigation( $data ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col col-lg-12">
                    <?php print $this->get_service_markup( $data ); ?>
                </div>
            </div>
            
        </div>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


