<?php

namespace Module;

defined( 'ABSPATH' ) ?: exit;

class Content_Section extends Module{
  
    const DATA_HANDLE           =   'page_introduction';
    const MODULE_HEADING        =   'module_heading';
    const MODULE_CONTENT        =   'module_content';

    const CONTAINER_SETTING     =   'container_width_setting';
    const CONTENT_SETTING       =   'content_setting';

    
    /**
     * Return the # Columns from Content Setting
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_column_class( $data ) 
    {
        // Set column class based on number of columns chosen
        switch ( $this->get( self::CONTENT_SETTING ) ) {
            case '3':
                return apply_filters( __METHOD__, 'columns__three' );  
                break;
            
            case '2':
                return apply_filters( __METHOD__, 'columns__two' ); 
                break;

            case '1':
                return apply_filters( __METHOD__, 'columns__one' ); 
                break;
                
            default: 
                return apply_filters( __METHOD__, 'columns__one' );
        }
    }
    

    /**
     * Return the Module content formated
     * @access  public 
     * @static
     * @param   array[mixed]   $data
     * @return  string
     */
    public function get_content( $data ) 
    {

        $heading = $this->get( self::MODULE_HEADING ) ? '<h2>' . $this->get( self::MODULE_HEADING ) . '</h2>' : null;
        $content = $this->get( self::MODULE_CONTENT ) ? $this->get( self::MODULE_CONTENT ) : null;
        
        ob_start(); ?>
        
        <section class="<?php echo $this->get( self::CONTAINER_SETTING );?>">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="module--heading">
                        <?php echo $heading; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-12">
                    <div class="module--content <?php echo $this->get_column_class( $data );?>">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </section>

        <?php
        return apply_filters( __METHOD__, ob_get_clean() );
    }
}


