<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class contains all TG specific functions so that they are namespaced
 */
class Functions {



    /**
     * Clean Numbers for tel: links
     * @param 	$number 	string
     * @param 	$echo 		bool
     * @return 				string
     */
    public static function clean_numbers( $number, $echo = true ) {
        $no_spaces = preg_replace("/[^a-z0-9]/i", "", $number);
        $letters2 = preg_replace("/[a-c]/i", "2", $no_spaces);
        $letters3 = preg_replace("/[d-f]/i", '3', $letters2);
        $letters4 = preg_replace("/[g-i]/i", '4', $letters3);
        $letters5 = preg_replace("/[j-l]/i", '5', $letters4);
        $letters6 = preg_replace("/[m-o]/i", '6', $letters5);
        $letters7 = preg_replace("/[p-s]/i", '7', $letters6);
        $letters8 = preg_replace("/[t-v]/i", '8', $letters7);
        $letters9 = preg_replace("/[w-z]/i", '9', $letters8);

        if( !$echo ) {
            return $letters9;

        } else {
            echo $letters9;
        }
    }



    /**
     * Create attributes from array
     * @access  public
     * @static 
     * @param   array       $attributes
     * @param   string      $callback
     */
    public static function create_attributes( $attributes, $callback = 'esc_attr' ) {
        if( empty( $attributes ) ) {
            return;
        }

        $attribute_array   =   [];

        foreach( $attributes as $key => $value ) {
            $attribute_array[]  =   $callback ? "$key=\"{$callback( $value )}\"": "$key=\"{$value}\"";
        }

        return join( ' ', $attribute_array );
    }



    /**
     * Format a button link
     * @access  public
     * @static
     * @param   array           $data
     * @param   array|string    $attrs      //Array of attributes $key => $value
     * @return  string
     */
    public static function get_button( $data = [], $attrs = [] ) {
        if( empty( $data['url'] ) && empty( $data['title'] ) ) {
            return;
        }

        $attributes     =   [
            'href'      =>  $data['url'],
            'target'    =>  !empty( $data['target'] ) ? $data['target']: '',
            'title'     =>  __( $data['title'], TG()->textdomain )
        ];

        if( is_string( $attrs ) ) {
            $attributes['class']    =   $attrs;

        } elseif( is_array( $attrs ) ) {
            $attributes     =   array_merge( $attributes, $attrs );
        }

        $button     =   sprintf(
            '<a %2$s>%1$s</a>',
            $data['title'],
            self::create_attributes( $attributes )
        );

        return apply_filters( __CLASS__.'/'.__FUNCTION__, $button );
    }


    /**
     * Format a button link
     * @access  public
     * @static
     * @param   array           $data
     * @param   array|string    $attrs      //Array of attributes $key => $value
     * @return  string
     */
    public static function get_address_block( $data = [] ) {

        ob_start(); ?>

        <span itemscope itemtype="http://schema.org/LocalBusiness">
            <meta itemprop="name" content="<?php echo get_bloginfo('name'); ?>">
            <meta itemprop="description" content="<?php echo get_bloginfo('description'); ?>">
            <a class="contact_address" href="<?php echo $data['map_link']; ?> " target="_blank" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress"><?php echo $data['street_address']; ?> </span><br/>
                <span itemprop="addressLocality"><?php echo $data['city']; ?> </span>,
                <span itemprop="addressRegion"><?php echo $data['state']; ?> </span>
                <span itemprop="postalCode"><?php echo $data['zip_code']; ?> </span>
            </a>
        </span>

        <?php
        return apply_filters( __CLASS__.'/'.__FUNCTION__, ob_get_clean() );
    }

    /**
     * Render the Page Builder
     * @access  public
     * @static
     * @param   array      $field_object
     * @uses    get_field
     * @return  html
     */
    public static function get_page_builder( $field_object = [] ) {
        if( empty( $field_object ) ) {
            return;
        }

        foreach( $field_object as $module ) {
            $content    =   \Module\Factory::get_module( $module );
            echo $content->get_html( $module );
        }
    }



    /**
     * Filter to wrap ™ ® © with superscript tag
     * @access  public
     * @static
     * @param   string      $text_to_filter
     * @return  html
     */
    public static function superscript_filter( $text ) {
        
        if (preg_match_all( '/(?:(?:(?:\™))|(?:(?:\®))|(?:(?:\©)))/', $text, $matches ) > 0) {
            
            foreach ($matches[0] as $index => $match) {
                $text   =   str_replace(
                    $match, 
                    "<sup>{$matches[0][$index]}</sup>",
                    $text
                );
            }
        
        }
        return $text;
    }

    /**
     * Filter to strip protocol, WWW and trailing slash from URLs.
     * @access  public
     * @static
     * @param   string      $text_to_filter
     * @return  html
     */
    public static function clean_urls( $text ) {
        
        //remove www http and https
        $text = preg_replace('/(?:(?:(?:www\.))|(?:(?:http\:\/\/))|(?:(?:https\:\/\/)))/', '', $text );
        
        //strip trailing slash if needed
        if(substr($text, -1) == '/') {
            $text = substr($text, 0, -1);
        }

        return $text;
    }





}