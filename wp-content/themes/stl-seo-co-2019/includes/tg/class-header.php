<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Header{

    const HEADER_SETTINGS            = 'header_settings';
    const HEADER_BUTTON              = 'header_button';
    const INTERNAL_HEADER_BUTTON     = 'header_button_internal_page';
    const WEBSITE_CREDITS            = 'website_credits';

    /**
     * Return Array of Theme Setting from options page
     * @access  public
     * @param   string   
     */
    public static function get_header_settings() {

        $header_settings = get_field( self::HEADER_SETTINGS , 'options');
        
        
        return apply_filters(__METHOD__, $header_settings );    
    }
    
    
    /**
     * Return Button to be used in the header
     * @access  private
     * @param   array   $data 
     */
    public static function render_button( $data ) {
        if (is_front_page()) {
            $button = Functions::get_button($data[self::HEADER_BUTTON], array('class' => 'button button--outline'));
        } else {
            $button = Functions::get_button($data[self::INTERNAL_HEADER_BUTTON], array('class' => 'button button--outline'));
        }
        
        
        return apply_filters(__METHOD__, $button );    
    }
    

	/**
	 * Return the Header HTML
	 * @access 	public
	 * @return 	string
	 */
	public static function get_html() {

        ob_start(); ?>
        
        <div class="contact--nav">
            <?php echo self::render_button( self::get_header_settings() ); ?>
            <?php echo Business_Info::get_phone_number( Business_Info::get_contact_info() ); ?>
            <?php echo Business_Info::get_email_link( Business_Info::get_contact_info() ); ?>
            <?php echo Business_Info::get_address_block( Business_Info::get_contact_info() ); ?>
        </div>

        <?php
		return ob_get_clean();
	}



	/**
	 * Build the Footer HTML
	 * @access 	public
	 * @return 	string
	 */
	public function build_html() {
		print $this->get_html();
	}



}