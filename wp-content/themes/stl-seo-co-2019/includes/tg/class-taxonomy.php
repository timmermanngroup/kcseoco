<?php
namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Taxonomy {

	/**
	 * The Taxonomy handle
	 * @var 	string
	 */
	private $handle;

	/**
	 * The assocaiated post_type(s)
	 * @var 	string/array
	 */
	private $post_type;



	/**
	 * Main Taxonomy Constructor
	 * @access 	public
	 * @param 	$handle 	string
	 * @param 	$args 		array
	 */
	public function __construct( $handle, $args ) {
		if( !$handle  || empty( $args ) ) {
			return;
		}

		$this->handle 		= 	strtolower( str_replace( array( ' ', '_' ), '-', $handle ) );
		$this->post_type 	= 	$args['post_type'];
		$args 				= 	$this->set_args( $args );

		register_taxonomy( $this->handle, $this->post_type, $args );	
	}



	/**
	 * Set Taxonomy Labels
	 * @access 	private
	 * @param 	$label_args 	array()
	 */
	private function set_labels( $label_args ) {
		if( empty( $label_args ) ) {
			return;
		}

		//Defaults
		$defaults 	= 	array(
			'name' 				=> 	'',
			'singular_name' 	=> 	''
		);

		//Merge defaults and given values
		$labels 	= 	array_merge( $defaults, $label_args );

		//Set class properties
		$this->name 			= 	$labels['name'];
		$this->singular_name 	= 	( isset( $labels['singular_name'] ) && $labels['singular_name'] != '' ) ? $labels['singular_name']: $this->name;

		//Establish returned labels
		$labels 	= 	array(
			'name'              => _x( $this->name, 'taxonomy general name', TG()->textdomain ),
			'singular_name'     => _x( 'Genre', 'taxonomy singular name', TG()->textdomain ),
			'search_items'      => __( 'Search ' . $this->name, TG()->textdomain ),
			'all_items'         => __( 'All ' . $this->name, TG()->textdomain ),
			'parent_item'       => __( 'Parent ' . $this->singular_name, TG()->textdomain ),
			'parent_item_colon' => __( 'Parent ' . $this->singular_name, TG()->textdomain ),
			'edit_item'         => __( 'Edit ' . $this->singular_name, TG()->textdomain ),
			'update_item'       => __( 'Update ' . $this->singular_name, TG()->textdomain ),
			'add_new_item'      => __( 'Add New ' . $this->singular_name, TG()->textdomain ),
			'new_item_name'     => __( 'New ' . $this->singular_name . ' Name', TG()->textdomain ),
			'menu_name'         => __( $this->singular_name, TG()->textdomain ),
		);

		return apply_filters( 'tg_taxonomy/set_labels', $labels, $this->handle ); 	
	}



	/**
	 * Set Taxonomy Args
	 * @access 	private
	 * @param 	$args 	array()
	 */
	private function set_args( $args ) {

		$labels 	= 	$this->set_labels( $args['labels'] );
		$args 		= 	( !isset( $args['args'] ) ) ? array(): $args['args'];

		//Setup Defaults
		$defaults 	= 	array(
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 
				'slug' 			=> $this->handle,
				'with_front'  	=> 	false,
				'hierarchical' 	=> 	true
			),
		);

		//Merge Defaults with given values
		$args 	= 	array_replace_recursive( $defaults, $args );

		//Setup Admin values
		$args['labels'] 		= 	$labels;

		return apply_filters( 'tg_taxonomy/set_args', $args, $this->handle );
	}
}