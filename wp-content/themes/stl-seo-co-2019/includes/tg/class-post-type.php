<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Post_Type {


	/**
	 * The Nice Name for the Post Type (usually plural)
	 * @var 	string
	 */
	protected $name;

	/**
	 * The Nice Singular Name
	 * @var 	string
	 */
	protected $singular_name;

	/**
	 * The query handle for the post type
	 * @var 	string
	 */
	private $handle;


	/**
	 * Main Post Type Constructor
	 * @access 		public
	 * @param 		$handle 	string
	 * @param 		$args 		array
	 */
	public function __construct( $handle, $args ) {
		if( !$handle || empty( $args ) ) {
			return;
		}

		$this->handle 	= 	strtolower( str_replace( array( ' ', '_' ), '-', $handle ) );
		$args 			= 	$this->set_args( $args );

		register_post_type( $handle, $args );
	}


	/**
	 * Scoped Getter for Handle.
	 * 
	 * @access protected
	 * @return string
	 */
	protected function getHandle() { 
		return $this->handle;
	}
	
	
	/**
	 * Scoped Setter for Labels
	 * 
	 * @access protected
	 * @param string $singular
	 * @param string $plural
	 * @return void
	 */
	protected function setLabelNames($singular, $plural) { 
		$this->name 			= 	$plural;
		$this->singular_name 	= 	$singular;
	}


	/**
	 * Set Post Type Labels
	 * @access 	private
	 * @param 	$label_args 	array()
	 */
	private function set_labels( $label_args ) {

		// Build Defaults from Handle
		$default_name_string 		= !empty( $label_args['singular_name'] ) ? $label_args['singular_name']: ucwords($this->getHandle(),"_");
		
		// Mostly works :3  - This allows you to replace the plural key without redefining everything, or guesses if nothing at all
		$default_name_string_plural = !empty($label_args['name']) ? $label_args['name'] : $default_name_string."s"; 

		$defaults 	= 	array(
			'name'                  => _x( $default_name_string_plural, 'Post Type General Name', TG()->textdomain ),
			'singular_name'         => _x( $default_name_string, 'Post Type Singular Name', TG()->textdomain ),
			'menu_name'             => __( $default_name_string_plural , TG()->textdomain ),
			'name_admin_bar'        => __( $default_name_string, TG()->textdomain ),
			'archives'              => __( "$default_name_string Archives", TG()->textdomain ),
			'attributes'            => __( "$default_name_string Attributes", TG()->textdomain ),
			'parent_item_colon'     => __( "Parent $default_name_string:", TG()->textdomain ),
			'all_items'             => __( "All $default_name_string_plural", TG()->textdomain ),
			'add_new_item'          => __( "Add New $default_name_string", TG()->textdomain ),
			'add_new'               => __( "Add New", TG()->textdomain ),
			'new_item'              => __( "New $default_name_string", TG()->textdomain ),
			'edit_item'             => __( "Edit $default_name_string", TG()->textdomain ),
			'update_item'           => __( "Update $default_name_string", TG()->textdomain ),
			'view_item'             => __( "View $default_name_string", TG()->textdomain ),
			'view_items'            => __( "View $default_name_string_plural", TG()->textdomain ),
			'search_items'          => __( "Search $default_name_string", TG()->textdomain ),
			'not_found'             => __( "Not found", TG()->textdomain ),
			'not_found_in_trash'    => __( "Not found in Trash", TG()->textdomain ),
			'featured_image'        => __( "Featured Image", TG()->textdomain ),
			'set_featured_image'    => __( "Set featured image", TG()->textdomain ),
			'remove_featured_image' => __( "Remove featured image", TG()->textdomain ),
			'use_featured_image'    => __( "Use as featured image", TG()->textdomain ),
			'insert_into_item'      => __( "Insert into $default_name_string", TG()->textdomain ),
			'uploaded_to_this_item' => __( "Uploaded to this $default_name_string", TG()->textdomain ),
			'items_list'            => __( "$default_name_string_plural list", TG()->textdomain ),
			'items_list_navigation' => __( "$default_name_string_plural list navigation", TG()->textdomain ),
			'filter_items_list'     => __( "Filter $default_name_string_plural list", TG()->textdomain ),
		);
		
		// Return Defaults if no labels are passed
		if( empty( $label_args ) ) {			
			$this->setLabelNames($defaults['singular_name'], $defaults['name']);
			return $defaults;
		}

		//Merge defaults and given values
		$labels = array_merge( $defaults, $label_args );

		// Refresh class properties (which could have been changed in the previous merge)
		$this->setLabelNames($labels['singular_name'], $labels['name']);
		
		return $labels; 	
	}


	/**
	 * Set Post Type Rewrite
	 * @access 	private
	 * @param 	$rewrite_args 	array()
	 */
	private function set_rewrite( $rewrite_args ) {

		$rewrites 	= 	( !$rewrite_args ) ? array(): $rewrite_args;

		//Setup the Defaults
		$defaults 	= 	array(
			'slug'                  => strtolower( str_replace( array( ' ', '_' ), '-', $this->name ) ),
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		);

		//Merge the defaults with the given values
		$rewrite 			= 	array_merge( $defaults, $rewrites );

		//Sanatize the slug if what was given was not permalink-friendly 
		$rewrite['slug'] 	= 	strtolower( str_replace( array( ' ', '_' ), '-', $rewrite['slug'] ) );

		return $rewrite;
	}


	/**
	 * Set Post Type Args
	 * @access 	private
	 * @param 	$args 	array()
	 */
	private function set_args( $args ) {

		//Retrieve teh values from the passed data
		$rewrite_args 	= 	( isset( $args['rewrite'] ) ) ? $args['rewrite']: array();
		$labels_args 	= 	!empty($args['labels']) ? $args['labels'] : array();

		//The the data through teh class methods
		$labels 	= 	$this->set_labels( $labels_args  );;
		$rewrite 	= 	$this->set_rewrite( $rewrite_args );
		$args 		= 	( !empty( $args['args'] ) ) ? $args['args']: false;

		//Setup Defaults
		$defaults 	= 	array(
			'supports'              => array( 'title', 'editor' ),
			'taxonomies'            => array(),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => strtolower( str_replace( array( ' ', '_' ), '-', $this->name ) ),
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page'
		);

		//If args are given..
		if( $args ) {
			//.. merge them with the defaults
			$args 	= 	array_replace_recursive ( $defaults, $args );

			//Otherwise set the to the defaults
		} else {
			$args 	= 	$defaults;
		}
		

		//Setup Admin values
		$args['label'] 			= 	__( $this->singular_name, TG()->textdomain );
		$args['description'] 	= 	__( $this->name, TG()->textdomain );
		$args['labels'] 		= 	$labels;
		$args['rewrite'] 		= 	$rewrite;

		return apply_filters( 'tg_post_type/set_args', $args, $this->handle );
	}


	/**
	 * Register the Post Type
	 * @access 	public
	 * @param 	$handle 	string
	 * @param 	$args 		array()
	 */
	public function register_post_type( $handle, $args ) {

		//Allow the args to be filtered
		$args 	= 	apply_filters( 'tg_post_type/register_args', $this->set_args( $args ) );		

		register_post_type( $this->handle, $args );
	}
}