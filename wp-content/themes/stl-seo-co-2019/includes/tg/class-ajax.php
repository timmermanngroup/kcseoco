<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Ajax {



	/**
	 * Hook in all the Ajax Methods
	 */
	static public function init() {

		add_action( 'wp_ajax_nopriv_foobar_ajax_request', array( 'TG\\Ajax', 'foobar_ajax_request' ) );
		add_action( 'wp_ajax_foobar_ajax_request', array( 'TG\\Ajax', 'foobar_ajax_request' ) );

	}



	/**
	 * Foobar Function
	 */
	public static function foobar_ajax_request() {

		//Verify teh nonce to make sure it is not malicous
		TG()->validate_nonce( $_REQUEST['nonce'], __FUNCTION__ );

		//Proceed with you code here...

		wp_send_json( 'Success' );
	}
}