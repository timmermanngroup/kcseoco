<?php
namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Fields {

    //const PAGE_BUILDER          =   'stlseo_page_builder';
    //const POST_BUILDER          =   'stlseo_post_builder';
    //const CASE_STUDY_BUILDER    =   'stlseo_case_study_builder';


    const PAGE_BUILDER          =   'page_builder';
    const POST_BUILDER          =   'post_builder';
    const CASE_STUDY_BUILDER    =   'case_study_builder';
    
}