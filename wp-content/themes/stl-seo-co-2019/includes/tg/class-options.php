<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Options {

	public static function init() {
		self::set_options_page();
		self::create_nav_menus();
		self::limit_editor_cap();
	}

	/**
	 * Create the Options Page
	 */
	public static function set_options_page() {		
		
		if( function_exists( 'acf_add_options_page' ) ) {

			$args 	= 	[
				'page_title' 	=> TG()->common_name . ' Options',
				'menu_slug' 	=> TG()->textdomain . '-options'

			];
			$args 	= 		apply_filters( __CLASS__ . '/' .__FUNCTION__ .' /args', $args );

			acf_add_options_page( $args );
		}

	}

	/**
	 * Create Menus
	 * @uses register_nav_menus()
	 */
	public static function create_nav_menus() {
		register_nav_menus(
			array(
                'primary' => esc_html__( 'Primary Menu', TG()->textdomain ),
                'footer'  => esc_html__( 'Footer Menu', TG()->textdomain ),
			)
		);
	}



	/**
	 * Limit the Editors Capabilties
	 * @access 	public
	 */
	public static function limit_editor_cap() {
		$role = get_role( 'editor' );
	    $role->add_cap( 'edit_theme_options' );
	    $role->remove_cap( 'switch_themes' );
	    $role->remove_cap( 'install_themes' );
	    $role->remove_cap( 'edit_themes' );
	}

}