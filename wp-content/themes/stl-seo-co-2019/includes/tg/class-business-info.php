<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Business_Info{

    const CONTACT_INFO  = 'contact_information';
    const PHONE         = 'phone_number';
    const EMAIL         = 'email_address';
    const MAP           = 'map_link';
    const ADDRESS       = 'street_address';
    const CITY          = 'city';
    const STATE         = 'state';
    const ZIP           = 'zip_code';

    /**
     * Return Array of Contact Info from options page
     * @access  public
     * @param   string   
     */
    public static function get_contact_info() {
        
        $contact_info = get_field( self::CONTACT_INFO , 'options');
        
        return apply_filters( __CLASS__.'/'.__FUNCTION__, $contact_info );    
    }


    
    /**
     * Return Phone Number Linked
     * @access  public
     * @static
     * @param   array           $data
     * @return  string
     */
    public static function get_phone_number( $data = [] ) {
        if( empty( $data[ self::PHONE ]) ) {
            return;
        }

        $clean_phone    = Functions::clean_numbers( $data[ self::PHONE ] , false );
        $phone_number   = '<a class="contact_phone" href="tel:' . $clean_phone . '">' . $data[ self::PHONE ] . '</a>';
        
        return apply_filters( __CLASS__.'/'.__FUNCTION__, $phone_number );    
    }
    
    
    
    /**
     * Return Email Linked
     * @access  public
     * @static
     * @param   array           $data
     * @return  string
     */
    public static function get_email_link( $data = [] ) {
        if( empty( $data[ self::EMAIL ]) ) {
            return;
        }

        $email_link   = '<a class="contact_email" href="mailto:' . $data[ self::EMAIL ] . '">' . $data[ self::EMAIL ] . '</a>';
        
        return apply_filters( __CLASS__.'/'.__FUNCTION__, $email_link );    
    }



    /**
     * Return Address Block
     * @access  public
     * @static
     * @param   array           $data
     * @return  string
     */
    public static function get_address_block( $data = [] ) {
        
        if( empty( $data ) ) {
            return;
        }

        ob_start(); ?>
        
        <span class="address" itemscope itemtype="http://schema.org/LocalBusiness">
            <a class="company_name" href="<?php echo get_bloginfo('url'); ?>"><span itemprop="name"><?php echo get_bloginfo('name'); ?></span></a><br/>
            <meta itemprop="description" content="<?php echo get_bloginfo('description'); ?>">
            <address>
            <a class="contact_address" href="<?php echo $data[ self::MAP ]; ?> " target="_blank" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress"><?php echo $data[ self::ADDRESS ]; ?> </span><br/>
                <span itemprop="addressLocality"><?php echo $data[ self::CITY ]; ?> </span>,
                <span itemprop="addressRegion"><?php echo $data[ self::STATE ]; ?> </span>
                <span itemprop="postalCode"><?php echo $data[ self::ZIP ]; ?> </span>
            </a>
            </address>
        </span>

        <?php
        return apply_filters( __CLASS__.'/'.__FUNCTION__, ob_get_clean() );
    }





}