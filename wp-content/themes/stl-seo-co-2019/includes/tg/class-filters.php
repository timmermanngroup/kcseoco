<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Filters {

    /**
	 * Main Actions/Filters
	 */
    const MAIN_BEFORE 	    = 	'tg/main/before';
    const MAIN_AFTER 	    = 	'tg/main/after';

	/**
	 * Main Loop Actions/Filters
	 */
	const MAIN_LOOP 		= 	'tg/main_loop';
	const MAIN_LOOP_BEFORE 	= 	'tg/main_loop/before';
	const MAIN_LOOP_AFTER 	= 	'tg/main_loop/after';

	/**
	 * Header Actions/Filters
	 */
	const HEADER_BEFORE 	= 	'tg/header/before';
	const HEADER_AFTER 		= 	'tg/header/after';
    
 	/**
	 * Navigation Actions/Filters
	 */   
    const NAVIGATION_BEFORE =   'tg/navigation/before';
    const NAVIGATION_START  =   'tg/navigation/start';
    const NAVIGATION_END    =   'tg/navigation/end';
    const NAVIGATION_AFTER  =   'tg/navigation/after';

    
    /**
	 * Header Actions/Filters
	 */
    const FOOTER_BEFORE 	= 	'tg/footer/before';


    

}