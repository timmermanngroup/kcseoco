<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Widgets {

	public static function init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', TG()->textdomain ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}

}