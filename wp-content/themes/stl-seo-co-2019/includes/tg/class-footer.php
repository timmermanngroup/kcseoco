<?php

namespace TG;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Footer{

    const FOOTER_SETTINGS   = 'footer_settings';
    const FOOTER_BUTTON     = 'footer_button';
    const WEBSITE_CREDITS   = 'website_credits';

    /**
     * Return Array of Theme Setting from options page
     * @access  public
     * @param   string   
     */
    public static function get_footer_settings() {
        
        $footer_settings = get_field( self::FOOTER_SETTINGS , 'options');
        
        return apply_filters( __CLASS__.'/'.__FUNCTION__, $footer_settings );    
    }
    
    
    /**
     * Return Button to be used in the footer
     * @access  private
     * @param   array   $data 
     */
    public static function render_button( $data ) {
        
        $button = Functions::get_button( $data[ self::FOOTER_BUTTON ] , array('class' => 'button button--outline d-md-none') );

        return apply_filters( __CLASS__.'/'.__FUNCTION__, $button );    
    }
    
    /**
     * Return Button to be used in the footer
     * @access  private
     * @param   array   $data   
     */
    public static function render_copyright( $data = [] ) {
        
        $copyright  = '<span class="credits"><span class="copyright">Copyright &copy; 2003 - ' . date('Y') . ' ' . get_bloginfo('name') . '</span>';
        
        if( ! empty( $data['website_credits']) ) {
            $copyright .= ' <span class="bar">|</span> <span class="backlink">Powered by <a target="_blank" href=' . $data[ self::WEBSITE_CREDITS ]['url'] .  '>' . $data[ self::WEBSITE_CREDITS ]['title'] . '</a></span>';
        }else{
            $copyright .= '</span></span>';
        }
        
        return apply_filters( __CLASS__.'/'.__FUNCTION__, $copyright );    
    }


	/**
	 * Return the Footer HTML
	 * @access 	public
	 * @return 	string
	 */
	public static function get_html() {

        ob_start(); ?>
        
        <footer class="footer">
            <div class="container-xl">
                <div class="row">
                    <div class="col-12 col-md-8 order-1">
                        
                        <?php echo \Component\Branding::get_markup( '/images/branding/stl-seo-logo.svg' ); ?>
                        <?php $args = array(
                            'theme_location' 	=> 	'footer', 
                            'menu_id' 			=> 	'footer-menu',
				            );
				            wp_nav_menu( $args );  
                        ?>
                        <?php echo self::render_button( self::get_footer_settings() ); ?>
                    
                    </div>
                    
                    <div class="col-12 col-md-4 order-2 order-md-4">
                        <?php echo Business_Info::get_phone_number( Business_Info::get_contact_info() ); ?>
                    </div>
                    
                    <div class="col-12 col-md-4 order-3 order-md-2">
                        <?php echo Business_Info::get_address_block( Business_Info::get_contact_info() ); ?>
                    </div>
                    
                    <div class="col-12 col-md-8 order-4 order-md-3">
                        <?php echo self::render_copyright( self::get_footer_settings() ); ?>
                    </div>

                </div>
            </div>
        </footer>
        
        <?php
		return ob_get_clean();
	}



	/**
	 * Build the Footer HTML
	 * @access 	public
	 * @return 	string
	 */
	public function build_html() {
		//print $this->get_html();
	}



}