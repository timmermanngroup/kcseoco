<?php
/*
* This file should contian all functions, actions, and filter pertaining
* to Gravity Forms
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Check to see if gravity forms is activated
if( function_exists( 'gravity_form' ) ):

/**
 * Let's limit the Editor's Capabilities eh.
 */
function tg_gravity_forms_limit_editor_cap() {
    $role = get_role( 'editor' );
    $role->remove_cap( 'gform_full_access' );
	$role->remove_cap( 'gravityforms_create_form' );
  	$role->remove_cap( 'gravityforms_delete_forms' );
  	$role->remove_cap( 'gravityforms_edit_forms' );

    $role->add_cap( 'gravityforms_view_entries' );
    $role->add_cap( 'gravityforms_export_entries' );
    $role->add_cap( 'gravityforms_delete_entries' );
    
}
add_action( 'admin_init', 'tg_gravity_forms_limit_editor_cap' );



/**
 * This adds the form name as a hidden field to each form for GA Tracking
 * @param 	$form 	array()
 */
function tg_ga_forms( $form ) {
	$form_title 				= 	$form['title'];
	$ga_field 					= 	new GF_Field_Hidden();
	$ga_field->cssClass 		= 	'ga-form';
	$ga_field->defaultValue 	= 	$form_title;
	$ga_field->visibility 		= 	'hidden';
	$form['fields'][] 			= 	$ga_field;
	
	return $form;
}
add_filter( 'gform_pre_render', 'tg_ga_forms' );



/**
 * This adds a class to each .gfield element that will identify what type of field it is
 * @param 	$form 	array()
 */
function tg_form_field_class( $form ) {
	$fields = $form['fields'];

	foreach( $fields as $field ) {
		$field->cssClass .= ' gfield_' . $field->type;
	}
	return $form;
}
add_filter( 'gform_pre_render', 'tg_form_field_class' );


endif; //End plugin activation check


