<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( defined( 'AIOSEOPPRO' ) ):

/**
 * Remove AIOSEO Meta boxes for Non-Admins
 */
function remove_aioseo_for_non_admin() {
    if( !current_user_can( 'manage_options' ) ) {
        $post_types = array( 'page', 'post' );

        foreach( $post_types as $type ) {
            remove_meta_box( 'aiosp', $type, 'normal' );
        }
    }
}
add_action( 'do_meta_boxes', 'remove_aioseo_for_non_admin' );

endif;