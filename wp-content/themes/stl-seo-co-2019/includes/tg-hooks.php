<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This hook allows for styles to be added to the HEAD tag
 * @param string $styles
 */
function tg_inline_styles( $styles ) {
	$styles = apply_filters( 'tg_inline_styles', $styles );
	$styles = '<style type="text/css">' . $styles . '</style>';

	echo $styles;
}
add_action( 'wp_head', 'tg_inline_styles', 500 );



/*
** Add Favicons
*/
function tg_favicons() { 
	$color = '#ffffff'; //Change this hex value to whatever you would like
	$template_path = get_template_directory_uri().'/images/favicons';
	?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $template_path; ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php echo $template_path; ?>/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo $template_path; ?>/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo $template_path; ?>/manifest.json">
<link rel="mask-icon" href="<?php echo $template_path; ?>/safari-pinned-tab.svg" color="#000000">
<link rel="shortcut icon" href="<?php echo $template_path; ?>/favicon.ico">
<meta name="msapplication-config" content="<?php echo $template_path; ?>/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
	<?php
}
add_action( 'wp_head', 'tg_favicons' );


/**
 * This function add the styles and scripts to the theme
 */
function tg_scripts() {
	//Deregister Styles/Scripts
	wp_deregister_script( 'jquery' );

	//Register Styles
	wp_register_style( TG()->textdomain, get_template_directory_uri() . '/css/style.css' );

	//Register Scripts
	wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js' );
	wp_register_script( TG()->textdomain, get_template_directory_uri() . '/js/base.js', array( 'jquery' ), null, true );

	//Enque Styles
	wp_enqueue_style( TG()->textdomain );

	//Enque Scripts
	wp_enqueue_script( TG()->textdomain );
}
add_action( 'wp_enqueue_scripts', 'tg_scripts' );








/**
 * Hook in Page Builders
 */
function cityname_content_builder() {
	global $post;
	$page_builder 			= 	get_field( \TG\Fields::PAGE_BUILDER, $post->ID );
	$post_builder 			= 	get_field( \TG\Fields::POST_BUILDER, $post->ID );
	$case_study_builder 	= 	get_field( \TG\Fields::CASE_STUDY_BUILDER, $post->ID );
	
	if( !is_singular() ) {
		return;
    }
    
    if( $page_builder ) {
    	\TG\Functions::get_page_builder( $page_builder );
    
    } else if ( $post_builder ){
		\TG\Functions::get_page_builder( $post_builder );
	
	} else if ( $case_study_builder ){
		\TG\Functions::get_page_builder( $case_study_builder );
		
	} else {
		get_template_part( 'template-parts/content');
	}		
}
add_action( \TG\Filters::MAIN_LOOP, 'cityname_content_builder', 10 );


/**
 * After Map Content
 */
function hh_after_locations_map_content() {
	global $post;
	$d_email 	= 	get_field( 'display_email' );
	$d_phone 	= 	get_field( 'display_phone' );
	$message 	= 	get_field( 'help_messaging' );
	$contact 	= 	get_field( 'contact', 'options' );
	$contact 	=  	array_filter( $contact );
	$message 	= 	( $message ) ? '<span class="text-h4 u-font-heading d-inline-block">' . $message . ' <i class="icon-right"></i></span>': '';

	if( empty( $contact ) ) {
		return;
	}

	if( $d_email || $d_phone ): ?>
<div class="row">

	<div class="col">
		<ul class="list--no-style u-font-700">
		<?php
			if( isset( $contact['phone'] ) && $d_phone ) {
				echo '<li><a href="tel:' . clean_numbers( $contact['phone'], false ) . '" >' . $contact['phone'] . '</a></li>';
			}
			if( isset( $contact['email'] ) && $d_email ) {
				echo '<li><a href="mailto:' . $contact['email'] . '">' . $contact['email'] . '</a></li>';
			}
		?>	
		</ul>
		
	</div>
</div>
	<?php endif;
}