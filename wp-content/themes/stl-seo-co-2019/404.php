<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package TG
 */

get_header(); 

$page_options_heading = get_option('options_heading') ?: null;
$page_options_hero_background = get_option('options_background_image') ?: null; 
$page_options_content = get_option('options_module_content') ?: null;
$page_options_button = get_option('options_module_button') ?: null;



?>

<main id="main" role="main" class="main">

    <?php 
        /**
        * Runs just inside the main element
        */
        do_action( \TG\Filters::MAIN_BEFORE );


        $hero_data = array(
            'heading'           => $page_options_heading,
            'background_image'  => $page_options_hero_background,
        );
        $basic_content_data = array(
            'module_content' => $page_options_content,
            'module_button'  => $page_options_button,
        );
        
        if ( empty( $basic_content_data )){
            return;
        }
    
        $hero_data['acf_fc_layout'] = 'hero';
        $hero = new \Module\Hero( $hero_data );
        echo $hero -> build_html();
        
        $basic_content_data['acf_fc_layout'] = 'basic_content';
        $basic_content = new \Module\Basic_Content( $basic_content_data );
        echo $basic_content -> build_html();

        
        /**
        * Runs just inside the close of the main element
        */
        do_action( \TG\Filters::MAIN_AFTER );
    ?>

</main><!-- #main -->

<?php get_footer(); ?>
