<?php
/**
 * The main template file.
 *
 *
 * @package TG
 */
get_header(); 

?>
<main id="main" class="main" role="main">

<?php 
    /**
	* Runs just inside the main element
	*/
    do_action( \TG\Filters::MAIN_BEFORE );


	if ( have_posts() ) {

		/**
		 * Runs just before the main loop
		 */
		do_action( \TG\Filters::MAIN_LOOP_BEFORE );

		while( have_posts() ) {
			the_post();
			do_action( \TG\Filters::MAIN_LOOP );

		}

		/**
		 * Runs just after the main loop
		 */
		do_action( \TG\Filters::MAIN_LOOP_AFTER );

    }
    
    /**
	* Runs just inside the close of the main element
	*/
    do_action( \TG\Filters::MAIN_AFTER );
?>

</main><!-- #main -->

<?php get_footer();
