<?php
/**
 *
 * @package TG
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class TG_Theme {

    /**
	 * Theme Common Name
	 * @var string
	 */
	public $common_name = 'NYC SEO Company';

	/**
	 * Text Domain for the theme
	 * @var string
	 */
	public $textdomain = 'nyc-seo-company';


	/**
	 * Create empty post types array
	 * @var array
	 */
	public $post_types = [];

	/**
	 * Create empty taxonomies array
	 * @var array
	 */
	public $taxonomies = [];


	/**
	 * The single instance of the class.
	 *
	 * @var TG_Theme
	 */
	protected static $_instance = null;



	/**
	 * Main TG_Theme Instance
	 * @access 	public
	 * @static
	 * @see 	TG()
	 * @return 	TG_Theme - Main Instance.
	 */
	public static function instance() {
		if( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}


	/**
	 * Main Constructor Method
	 * @access 	public
	 * @return 	void
	 */
	public function __construct() {

		//Allow classes to be called automatically
		spl_autoload_register( 'TG_Theme::autoload_class' );

		//Set ACF Fields Path
		$this->set_acf_fields_path();

		//Include any non-object oriented files
		$this->includes();

		//Clean up and remove unecessary functions
		$this->wp_cleanup();
		
		//Run the Setup once the theme is ready
		add_action( 'init', array( $this, 'setup' ) );
		add_action( 'init', array( 'TG\\Ajax', 'init' ) );

		//Add Theme options
		add_action( 'after_setup_theme', array( 'TG\\Options', 'init' ) );		
	}



	/**
	 * Runs intial setup Methods
	 * @access 	public
	 * @return 	void
	 */
	public function setup() {

		load_theme_textdomain( $this->textdomain, get_template_directory() . '/languages' );
		
		add_action( 'widgets_init', array( 'TG\\Widgets', 'init' ) );
		add_filter('admin_footer_text', array( $this, 'admin_footer' ) );

		$this->add_theme_support();
		$this->register_post_types();
		$this->register_taxonomies();
		$this->add_image_sizes();	
	}



	/**
	 * Set the ACF Fields Path
	 * @access 	private
	 * @return 	void
	 */
	private function set_acf_fields_path() {

		if( !defined( 'ACF_FIELDS_PATH' ) ) {
			define( 'ACF_FIELDS_PATH', dirname( __FILE__ ) . '/includes/fields' );
		}		
	}




	/**
	 * Bring in all the includes
	 * @access 	private
	 * @return 	void
	 */
	private function includes() {

		//Pull in hooks
        include_once( 'includes/tg-hooks.php' );
        include_once( 'includes/custom-hooks.php' );
        

		//Pull in functions
		include_once( 'includes/tg-template-tags.php' );
		
		//plugin functions
		include_once( 'includes/tg-aioseo-functions.php' );
		include_once( 'includes/tg-gravity-forms-functions.php' );

		$this->acf_includes();
	}



	/**
	 * Include all ACF local fields
	 * @access 	private
	 */
	private function acf_includes() {
		if( !function_exists('acf_add_local_field_group') ) {
			return;
		}

		$directories 	= 	[
			'templates' 	=> 	ACF_FIELDS_PATH . '/templates',
			'modules' 		=> 	ACF_FIELDS_PATH . '/modules',
			'fields' 		=> 	ACF_FIELDS_PATH
		];
		
		foreach( $directories as $directory ) {
			
			//If the path is not valid
			if( !file_exists( $directory ) ) {
				continue;
			}

			$directory_object 	= 	new DirectoryIterator( $directory );
	
			foreach( $directory_object as $file_info ) {

				if( !$file_info->isFile() || $file_info->getExtension() != 'php' ) {
					continue;
				}

				$file_path 	= 	"{$directory}/{$file_info->getFilename()}";

				include_once( $file_path );
			}			
		}

	}



	/**
	 * Clean Up all the un-needed thing WP does
	 * @access 	private
	 * @return 	void
	 */
	private function wp_cleanup() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'wp_generator' );

		//Sanatize the resources
		add_action( 'script_loader_src', array( $this, 'remove_script_versions' ), 15 );
		add_action( 'style_loader_src', array( $this, 'remove_script_versions' ), 15 );
	}



	/**
	 * Remove version numbers from scripts and styles.
	 * Also load resouces agnostic to HTTP 
	 * @access 	private
	 * @static
	 * @param 	string		$src
	 * @return 	string
	 */
	public static function remove_script_versions( $src ) {
		$parts 	= 	explode( 'ver=?', $src );

		return str_replace( array( 'http:', 'https:' ), '', $parts[0] );
	}



	/**
	 * Add Theme Support
	 * @access 	public
	 * @return 	void
	 */
	public function add_theme_support() {

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 
			'html5', 
			array( 
				'search-form', 
				'gallery', 
				'caption'
			) 
		);
		add_theme_support( 'post-thumbnails' );

		//Allow for support to be hooked
		do_action( __CLASS__ . '/' . __FUNCTION__ );
	}



	/**
	 * Remove the additional CSS section, introduced in 4.7, from the Customizer.
	 * @access 	private
	 * @param 	WP_Customize_Manager 	$wp_customize
	 * @return 	void
	 */
	private function prefix_remove_css_section( $wp_customize ) {
		$wp_customize->remove_section( 'custom_css' );
	}



	/**
	 * Set the Post Types for the theme
	 * @access public
	 */
	 public function register_post_types() {
        
        /*$this->post_types['post_type_handle'] 	= 	array(
	 		'labels' 	=> 	array(
	 			'name' 				=> 'Post Types',
	 			'singular_name' 	=> 	'Singular Name'
	 		),
	 		'args' 		=> 	array(
	 			'taxonomies' 	=> array( 'tax_handle' )
	 		),
	 		'rewrite' 	=> 	array()
	 	);*/
        

        $this->post_types[ 'portfolio' ] 	= 	array(
	 		'labels' 	=> 	array(
	 			'name' 				=>  'Portfolio',
	 			'singular_name' 	=> 	'Portfolio'
	 		),
	 		'args' 		=> 	array(
                'has_archive'   => false,
                'menu_icon'     => 'dashicons-images-alt',
                'taxonomies' 	=> array( 'tax_handle' ),
                'hierarchical' => true,
                'supports' => array('title', 'custom-fields', 'page-attributes')

	 		),
	 		'rewrite' 	=> 	array()
         );                                                               
        
        $this->post_types[ 'testimonial' ] 	= 	array(
	 		'labels' 	=> 	array(
	 			'name' 				=>  'Testimonials',
	 			'singular_name' 	=> 	'Testimonial'
	 		),
	 		'args' 		=> 	array(
                'has_archive'   => false,
                'menu_icon'     => 'dashicons-format-quote',
	 			'taxonomies' 	=> array( 'tax_handle' ),
	 		),
	 		'rewrite' 	=> 	array()
        );
        
        $this->post_types[ 'case-study' ] 	= 	array(
	 		'labels' 	=> 	array(
	 			'name' 				=>  'Case Studies',
	 			'singular_name' 	=> 	'Case Study'
	 		),
	 		'args' 		=> 	array(
                'show_in_nav_menus' => true,
                'has_archive'       => 'our-work/case-studies',
                'menu_icon'         => 'dashicons-clipboard',
                'taxonomies' 	    => array( 'industry' ),
                'rewrite'           => array( 'slug' => 'case-studies' ),
	 		),
	 		'rewrite' 	=> 	array()
	 	);

		// Allow for other areas to apply filters to teh post types
		$post_types 	= 	apply_filters( 'tg/post_type/register', $this->post_types );

		foreach( $post_types as $handle => $args ) {
			$post_type 	= 	new \TG\Post_Type( $handle, $args );
		}
	 }



	/**
	 * Set the Taxonomies for the theme
	 * @access 	public
	 * @return 	void
	 */
	 public function register_taxonomies() {

	 	/*$this->taxonomies['tax_handle'] 	= 	array(
	 		'post_type' 	=> 	'post_type_handle',
	 		'labels' 		=> 	array(
	 			'name' 			=> 	'Terms',
	 			'singular_name' => 	'Term'
	 		)
	 	);*/
         
        $this->taxonomies['industry'] 	= 	array(
	 		'post_type' 	=> 	'case-study',
	 		'labels' 		=> 	array(
	 			'name' 			=> 	'Industry',
	 			'singular_name' => 	'Industry'
	 		)
	 	);


	 	// Allow for the taxonomies to be filtered
	 	$taxonomies 		= 	$this->taxonomies;
	 	$this->taxonomies 	= 	apply_filters( 'tg/taxonomies/register', $taxonomies );

	 	foreach( $this->taxonomies as $handle => $args ) {
			$taxonomy 	= 	new \TG\Taxonomy( $handle, $args );
		}

	 }


	 /**
	  * Set Custome Image sizes for the theme
	  * @access 	private
	  * @return 	void
	  */
	 private function add_image_sizes() {
         //add_image_size( $name, $width, $height, $crop );
        add_image_size( 'card' , 315 , 250, true );
        add_image_size( 'med-card' , 325 , 325, true );
        add_image_size( 'large-card' , 500 , 500, true );
        add_image_size( 'banner' , 1340 , 410, true );
     }



	/**
	 * Give TG Credit for the website
	 * @access 	public
	 * @return 	void
	 */
	 public function admin_footer() {
	 	return 'Site designed and developed by <a href="http://www.wearetg.com/" target="_blank">Timmermann Group</a> and powered by <a href="http://wordpress.org" target="_blank">WordPress</a>.';
	 }


	/**
	 * AutoLoad Classes
	 * @access 	public
	 * @static
	 * @param 	string 		$class_name
	 * @return 	void
	 */
	public static function autoload_class( $object_name ) {

		$object_types     =   [ 'class', 'trait', 'interface' ];
		$files            =   [];

		//If the Class is namespaced
		if( preg_match( '/\\\\/', $object_name, $match ) == 1 ) {
			$directories    =   array_values( array_filter( explode( '\\', $object_name ) ) );       
			$class          =   array_pop( $directories );
			$class          =   strtolower( str_replace( '_', '-', $class ) );

			foreach( $object_types as $object_type ) {
				$files[]  =   strtolower( join( '/', $directories ) ) . "/{$object_type}-{$class}.php";
			}
			

		} else {
			$class  =   strtolower( str_replace( '_', '-', $object_name ) );
			
			foreach( $object_types as $object_type ) {
				$files[]  =   "{$object_type}-{$class}.php";
			}
		}

		foreach( $files as $file ) {
			
			if( file_exists( dirname( __FILE__ ) . "/includes/{$file}" ) ) {
				include_once( dirname( __FILE__ ) . "/includes/{$file}" );
			}
		}
	}



	/**
	 * Validate Nonce
	 * @access 	public
	 * @param 	string 	$nonce
	 * @param 	string 	$action
	 * @return 	void
	 */
	public static function validate_nonce( $nonce, $action ) {

		if( !wp_verify_nonce( $nonce, $action ) ) {
			$notice 	= 	apply_filters( __CLASS__.'/'.__FUNCTION__.'/notice', "Nonce isn't valid.", $action );
			wp_die( $notice );
		}
	}

}

/**
 * Main instance of TG_Theme.
 * @return TG_Theme
 */
function TG() {
	return TG_Theme::instance();
}

$GLOBALS['tg'] = new TG_Theme();