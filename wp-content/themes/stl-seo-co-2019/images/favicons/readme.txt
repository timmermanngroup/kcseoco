Step 1:

Go to http://realfavicongenerator.net/ and follow the steps for generating your favicon

Step 2: 

UnZip the images file and place all images in this directory

Step 3:

You're done!

Step 4(Optional):

If you wish to change the theme color for your Microsft Tile, open the file located at
/%TEMPLATE_DIRECTORY%/inc/theme-functions.php, and change the Hex code for the color