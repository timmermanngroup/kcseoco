<?php
/**
 * The header for our theme.
 * @package TG
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>
<body <?php body_class('drawer drawer--top'); ?>>


<?php do_action( \TG\Filters::HEADER_BEFORE ); ?>

<header id="drawer-nav" class="drawer-navbar" role="banner">
    <div class="container-xl">
        <div class="drawer-navbar-header">
            
            <?php echo \Component\Branding::get_markup( '/images/branding/stl-seo-logo.svg' ); ?>
            
            <button type="button" class="drawer-toggle drawer-hamburger">
		      <span class="sr-only">toggle navigation</span>
		      <span class="drawer-hamburger-icon"></span>
		    </button>
		</div>

        <?php do_action( \TG\Filters::NAVIGATION_BEFORE ); ?>
        
            <nav class="drawer-nav" role="navigation">

                <?php do_action( \TG\Filters::NAVIGATION_START ); ?>
                
                    <?php $args = array(
                            'theme_location' 	=> 	'primary', 
                            'menu_id' 			=> 	'primary-menu',
                            'menu_class' 		=> 	'drawer-menu mr-auto'
				        );
				        wp_nav_menu( $args );  
                        ?>
                <?php echo TG\Header::get_html(); ?>

            </nav>
        
            <?php do_action( \TG\Filters::NAVIGATION_AFTER ); ?>
    </div>
</header>


<?php do_action( \TG\Filters::HEADER_AFTER );