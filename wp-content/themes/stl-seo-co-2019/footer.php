<?php
/**
 * The template for displaying the footer.
 *
 * @package TG
 */

do_action( \TG\Filters::FOOTER_BEFORE );

echo TG\Footer::get_html(); ?>


<?php wp_footer(); ?>

</body>
</html>
