<?php
/**
 * Template Name: Basic Page Template
 *
 * This is an example of a page template. Rename this file whatever you
 * wish.
 *
 * @package TG
 */

get_header(); 

?>
<main id="main" role="main">

<?php 
	if ( have_posts() ) {

		/**
		 * Runs just before the main loop
		 */
		do_action( 'tg/main_loop/before' );

		while( have_posts() ) {
			the_post();
			get_template_part( 'template-parts/content' );

		}

		/**
		 * Runs just after the main loop
		 */
		do_action( 'tg/main_loop/after' );

	}
?>

</main><!-- #main -->

<?php get_footer();