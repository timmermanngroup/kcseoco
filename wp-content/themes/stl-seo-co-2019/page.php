<?php
/**
 * The template for displaying all pages.
 *
 * @package TG
 */

get_header(); 

?>
<main id="main" class="main" role="main">

    <?php do_action( \TG\Filters::MAIN_BEFORE ); ?>
    
    <?php

	if ( have_posts() ) {

		/**
		 * Runs just before the main loop
		 */
		do_action( \TG\Filters::MAIN_LOOP_BEFORE );

		while( have_posts() ) {
			the_post();
			do_action( \TG\Filters::MAIN_LOOP );

        }
        
        /**
		 * Runs just after the main loop
		 */
		do_action( \TG\Filters::MAIN_LOOP_AFTER );

	}
?>

</main><!-- #main -->

<?php get_footer();