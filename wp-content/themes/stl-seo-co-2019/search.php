<?php
/**
 * The template for displaying search results pages.
 *
 * @package TG
 */

get_header(); 

?>
<main id="main" role="main">

<?php 
	/**
	 * Runs just before the main loop
	 */
	do_action( 'tg/main_loop/before' );

	if ( have_posts() ) {		

		while( have_posts() ) {
			the_post();
			get_template_part( 'template-parts/content', 'search' );

		}

	} else {
		get_template_part( 'template-parts/content', 'none' );
	}

	/**
	 * Runs just after the main loop
	 */
	do_action( 'tg/main_loop/after' );
?>

</main><!-- #main -->
