To chnage the name of the theme:

Preform a search and replace of the word 'foobar' inside the foobar folder and replace it with what ever you want the slug of your new theme to be. 
*NO SPACES, NO HYPHENS, NO NUMBERS

After performing a search and replace, change the name of the folder from foobar to whatever the slug of your new theme is called.

Enjoy.