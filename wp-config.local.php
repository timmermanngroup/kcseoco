<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kcseoco');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+^)tk@JmE?~1<h?:^$Qfm|OF;^9WsXOIG6z8rx`@o2k~;s+b+PYib%[+p a<&Wo<');
define('SECURE_AUTH_KEY',  '7(rpPby(,j|}|.($p4),eB/tM8(;[ht Cw;g*v}u&gN3Fp8-zJD$5o27^O[.h9NA');
define('LOGGED_IN_KEY',    '${NK>S1~[Lo#W>l66+W?4UDP-4bQWqgf-R#8Y:l+2 #aLh|;qq@/~%ZQFvUZI~>U');
define('NONCE_KEY',        '|DA/WcGp`c`9Cn`D$]@yt5o$mv4>M|&Dk*`}1.fL]$blQ_u|<HvYy;Gk=m6:l[+D');
define('AUTH_SALT',        'dd?Y+^+ W];L;x+_L}aD,Dr[OL<{<BrnK!JBQBj^)36T2W^<=>ZLy;+_E azx3Gz');
define('SECURE_AUTH_SALT', '5Z*?/Eo-C|YjoimA+Na1)27wfZq2-Dv7,%jBtS|SZvBa^VaGU_vr5L^Qd|Wsv;==');
define('LOGGED_IN_SALT',   ')@]/a90LXElPe|`7(sJRd%s|S6_V4P]nID>g8_2502_|x,KEMl-DptBty1}QD!?C');
define('NONCE_SALT',       '|D1MVfSmDIfu-U7@ceBc?LvwhxYn.Dxl}5EWs8Pd$,ql^]Jvz6%iv%?``n[r2O6M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define( 'WP_HOME', 'http://kcseoco.localhost' );
define( 'WP_SITEURL', 'http://kcseoco.localhost/' );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
